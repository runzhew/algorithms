#include <stdio.h>
#include <stdlib.h>

typedef struct _node {
	int val;
	struct _node *prev;
	struct _node *next;
}node;

typedef struct _list {
	node * head;
	node * tail;
	long len;
}list;

list * create(void)
{
	list *l = (list *)malloc(sizeof(list));

	l->head = l->tail = NULL;
	l->len = 0;
}

void add_head(list *l, int val)
{
	node * n = (node *) malloc(sizeof(node));

	n->val = val;
	if (l->len == 0) {
		l->head = l->tail = n;
		n->prev = NULL;
		n->next = NULL;
	}
	else {
		n->prev = NULL;
		n->next = l->head;
		l->head->prev = n;
		l->head = n;		
	}
	l->len++;
}

void add_tail(list *l, int val) 
{
	node *n = (node *) malloc(sizeof(node));

	n->val = val;
	if (l->len == 0) {
		n->prev = n->next = NULL;
		l->head = n;
		l->tail = n;
	}
	else {
		l->tail->next = n;
		n->prev = l->tail;
		n->next = NULL;
		l->tail = n;
	}
	l->len++;
}

void dump(list *l) 
{
	node * n = l->head;
	while (n != NULL) {
		printf("%d, ", n->val);
		n = n->next;
	}
	printf("\n");
}

int main()
{
	list *ll = create();
	add_head(ll, 13);
	add_head(ll, 12);
	add_head(ll, 11);
	add_head(ll, 10);
	add_head(ll, 9);
	add_head(ll, 8);
	add_head(ll, 7);
	add_head(ll, 6);
	add_head(ll, 5);
	add_head(ll, 4);
	add_head(ll, 3);
	dump(ll);

	printf("len %ld\n", ll->len);

	return 0;
}
