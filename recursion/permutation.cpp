#include <iostream>
#include <string>
#include <vector>

using namespace std;

void _permutation(int start, string &str, vector<string> &result)
{
	if (start == str.size()-1) {
		result.push_back(str);
		return ;
	}

	for (int k = start; k < str.size(); k++) {
		swap(str[k], str[start]);
		_permutation(start+1, str, result);
		swap(str[k], str[start]);
	}
}

vector<string> permutation() 
{
	string str = "123456789";
	vector<string> result;
	_permutation(0, str, result);

	return result;
}

int main()
{
	vector<string> res = permutation();
	cout << "total num = " << res.size() << endl;
	//for (int i = 0; i < res.size(); i++) {
		//cout << res[i] << endl;
	//}
	return 0;
}
