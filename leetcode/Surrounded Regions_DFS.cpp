#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
	void solve(vector<vector<char> > &board) {
		if (board.size() == 0)
			return ;

		m = board.size();
		n = board[0].size();

		for (int i = 0; i < n; i++) {
			bfs(board, 0, i);
			bfs(board, m-1, i);
		}

		for (int i = 0; i < m; i++) {
			bfs(board, i, 0);
			bfs(board, i, n-1);
		}

		dump(board);

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (board[i][j] == '+') {
					board[i][j] = 'O';
				}
				else if (board[i][j] == 'O') {
					board[i][j] = 'X';
				}
			}
		}

	}

private:
	int m, n; // for board

	void bfs(vector<vector<char> > &board, int i, int j)
	{
		if (i < 0 || i >= m || j < 0 || j >= n || board[i][j] != 'O') {
			return;
		}


		if (board[i][j] == 'O') {
			board[i][j] = '+';

			bfs(board, i, j+1);
			bfs(board, i, j-1);
			bfs(board, i+1, j);
			bfs(board, i-1, j);
		}
	}

	void dump(vector<vector<char> > &board)
	{
		for (int i = 0; i < board.size(); i++) {
			for (int j = 0; j < board[0].size(); j++) {
				cout << board[i][j] << " , ";
			}
			cout << endl;
		}
		cout << endl;
	}

};


void dump(vector<vector<char> > &board)
{
	for (int i = 0; i < board.size(); i++) {
		for (int j = 0; j < board[0].size(); j++) {
			cout << board[i][j] << " , ";
		}
		cout << endl;
	}
}


int main()
{
	Solution sol;
	vector<vector<char> > board;
	vector<char> tmp;

	tmp.push_back('X');
	tmp.push_back('X');
	tmp.push_back('X');
	tmp.push_back('X');
	board.push_back(tmp);
	tmp.clear();

	tmp.push_back('X');
	tmp.push_back('O');
	tmp.push_back('O');
	tmp.push_back('X');
	board.push_back(tmp);
	tmp.clear();

	tmp.push_back('X');
	tmp.push_back('X');
	tmp.push_back('O');
	tmp.push_back('X');
	board.push_back(tmp);
	tmp.clear();

	tmp.push_back('X');
	tmp.push_back('O');
	tmp.push_back('X');
	tmp.push_back('X');
	board.push_back(tmp);
	tmp.clear();

	sol.solve(board);
	dump(board);

	return 0;
}
