class Solution {
public:
	int maximalRectangle(vector<vector<char> > &matrix) {

		int max = 0;
		if (matrix.size() == 0)
		    return 0;
		vector<int> height(matrix[0].size(), 0);

		for (int i = 0; i < matrix.size(); i++) {
			setup_histogram(i, matrix, height);
			int t = largestRectangleArea(height);
			if (max < t) {
				max = t;
			}
		}

		return max;

	}

private:
	int largestRectangleArea(vector<int> &height) {

		int max = 0;

		for (int i = 0; i < height.size(); i++) {

			if (i+1 < height.size() && height[i] <= height[i+1]) {
				continue;
			}

			int minHeight = height[i];
			for (int j = i; j >= 0; j--) {
				if (height[j] < minHeight) {
					minHeight = height[j];
				}
				int tmp_max = minHeight * (i-j+1);

				if (tmp_max > max)
					max = tmp_max;
			}
		}
		return max;
	}

	void setup_histogram(int i, vector<vector<char> > &matrix, vector<int> &height)
	{
		for (int j = 0; j < matrix[i].size(); j++) {
			int count = 0;
			if (matrix[i][j] == '1') {
				for (int k = i; k >= 0 && matrix[k][j] != '0'; k--) {
					count ++;
				}
				height[j] = count;
			}
			else {
				height[j] = 0;
			}
		}

	}
};
