#include <iostream>
#include <vector>
#include <set>

using namespace std;

class Solution {
public:
	vector<vector<int> > permuteUnique(vector<int> &num) {
		sort(num.begin(), num.end());

		vector<vector<int> > result;
		vector<int> tmp;

		map<int, int> m;

		// statistic how many times a number appears
		for (int i = 0; i < num.size(); i++) {
			if (m.find(num[i]) == m.end()) {
				m[num[i]] = 1;
			}
			else {
				m[num[i]]++;
			}
		}

		// construct a vector<pair<int, int>>
		vector<pair<int, int> > numbers;
		for (auto it = m.begin(); it != m.end(); it++) {
			numbers.push_back(*it);
		}

		int n = num.size();
		dfs(numbers, result, tmp, n); 

		return result;
	}

	void dfs(vector<pair<int, int> > &numbers, vector<vector<int> > &result,
		 vector<int> tmp, int n) {

		if (tmp.size() == n) {
			result.push_back(tmp);
			return ;
		}

		for (auto it = numbers.begin(); it != numbers.end(); it++) {
			int count = 0;
			for (int j = 0; j < tmp.size(); j++) {
				if (it->first == tmp[j])
					count++;
			}

			if (count < it->second) {
				tmp.push_back(it->first);
				dfs(numbers, result, tmp, n);
				tmp.pop_back();
			}
		}
	}
};

int main()
{
	Solution sol;
	int t[] = {1,1,2,2,2,1};
	vector<int> num(t, t+6);

	sol.permuteUnique(num);
	sol.dump();

	return 0;
}
