#include <iostream>

using namespace std;

class Solution {
public:
	bool isPalindrome(int x) {
	    
	    if (x < 0) 
		    return false;

	    int x1 = x;
	    int d = 1;
	    while (x1/10 != 0) {
		    d *= 10;
		    x1 /= 10;
	    }

	    int head, rear;

	    while (x != 0) {
		    head = x / d;
		    rear = x % 10;
		    if (head != rear) 
			    return false;
		    x = x % d; //delete head
		    x /= 10; // delete rear

		    d /= 100;
	    }
	    return true;
	}

};

class Solution {
public:
	bool isPalindrome(int x) {
		int d = 1;
		int head, rear;

		if (x < 0)
			return false;

		while (x/d >= 10) {
			d *= 10;
		}

		cout << "Sol1: d=  " << d << endl;

		while (x > 0) {
			head = x / d;
			rear = x % 10;
			if (head != rear)
				return false;

			x = x % d;
			x = x / 10;
			cout << "Sol1: " << x << endl;

			d = d / 100;
		}

		return true;

	}
};

class Solution2 {
	public:
		bool isPalindrome(int x) {
			if (x < 0) return false;
			int d = 1; // divisor
			while (x / d >= 10) 
				d *= 10;
			cout << "Sol2: d=  " << d << endl;

			while (x > 0) {
				int q = x / d;
				int r = x % 10;
				if (q != r) return false;
				x = x % d / 10;
				cout << "Sol2: " << x << endl;
				d /= 100;
			}
			return true;


	}
};

int main()
{
	Solution sol1;
	Solution2 sol2;
	cout << sol1.isPalindrome(1001) << endl;
	cout << sol2.isPalindrome(1001) << endl;
	
	return 0;
}

