/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<int> preorderTraversal(TreeNode *root) {
        
        stack<TreeNode*> st;
        vector<int> res;
        while (root != NULL) {
            res.push_back(root->val);
            st.push(root);
            root = root->left;
        }
        
        TreeNode *t;
        while (!st.empty()) {
            t = st.top();
            st.pop();
            
            if (t->right != NULL) {
                t = t->right;
                while (t != NULL) {
                    res.push_back(t->val);
                    st.push(t);
                    t = t->left;
                }
            }
        }
        return res;
    }
};


class Solution {
public:
	vector<int> preorderTraversal(TreeNode *root) {
		stack <TreeNode*> st;
		vector <int> v;
		TreeNode * p;

		if (root == NULL)
			return v;
		st.push(root);
		while(!st.empty()) {
			p = st.top();
			st.pop();
			v.push_back(p->val);
			if (p->right != NULL) {
				st.push(p->right);
			}
			if (p->left != NULL) {
				st.push(p->left);
			}
		}

		return v;
	}

};
