#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
	string getPermutation(int n, int k) {

		vector<int> note(n, 1);
		string t_str = "123456789";
		string str = t_str.substr(0, n);
		string ret;

		for (int i = 1; i < n; i++) { // calculate 1! 2! 3! .... (n-1)!
			note[i] = note[i-1] * i;
		}

		int index;
		k--; //非常重要，没有 k-- 是错的
		
		for (int j = n-1; j >= 0; j--) {

			index = k / note[j]; // because str begins from 0, so, don't need +1
			k = k % note[j];
			
			ret += str[index];
			str.erase(index, 1);
		}

		return ret;
	}
};



int main()
{
	Solution sol;

	cout << sol.getPermutation(4, 9) << endl;

	return 0;
}
