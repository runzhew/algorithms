/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	int minDepth(TreeNode *root) {

		min = INT_MAX;
		traveler(root, 1);
		return min;

	}
private:
	int min;

	void traveler(TreeNode *root, int h)
	{
		if (root->left == NULL && root->right == NULL) { // leaf
			if (min > h)
				min = h;
			return;
		}

		if (root->left != NULL) {
			h++;
			traveler(root->left, h);
		}

		if (root->right != NULL) {
			h++;
			traveler(root->right, h);
		}
	}
};
