#include <iostream>
#include <vector>
#include <string>
#include <set>

using namespace std;

class Solution {
public:
	vector<string> findRepeatedDnaSequences(string s) {

		set<string> save;
		vector<string> res;
		string tmp;

		for (int i = 0; i < s.size()-10; i++) {
			tmp = s.substr(i, 10);
			if (save.find(tmp) == save.end()) {
				save.insert(tmp);
				continue;
			}
			else {
				res.push_back(tmp);
			}
		}
		return res;
	}
};

int main()
{
	Solution sol;
	vector<string> res = sol.findRepeatedDnaSequences("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT");
	for (int i = 0; i < res.size(); i++) {
		cout << res[i] << endl;
	}

	return 0;
}
