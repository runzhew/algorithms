#include <iostream>

using namespace std;

struct ListNode {
	int val;
	ListNode * next;
	ListNode(int x): val(x), next(NULL) {}
};

void dump(ListNode *p) {

	while (p != NULL) {
		cout << p->val << ", ";
		p = p->next;
	}

	cout << endl;
}

class Solution {
public:
	ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {

		int  carry = 0;
		ListNode *output = new ListNode(0);
		ListNode * p = output;
		
		while (l1 != NULL && l2 != NULL) {
			int tmp = l1->val + l2->val + carry;
			if (tmp >= 10) {
				carry = 1;
				tmp = tmp % 10;
			}
			else {
				carry = 0;
			}

			// cout  << "tracepoint 1" << endl;
			p->next = new ListNode(tmp);
			p = p->next;

			l1 = l1->next;
			l2 = l2->next;
		}

		while (l1 != NULL) {
			int tmp = l1->val + carry;
			if (tmp >= 10) {
				carry = 1;
				tmp = tmp % 10;
			}
			else {
				carry = 0;
			}
			p->next = new ListNode(tmp);
			p = p->next;
			l1 = l1->next;
		}

		while (l2 != NULL) {
			int tmp = l2->val + carry;
			if (tmp >= 10) {
				carry = 1;
				tmp = tmp % 10;
			}
			else {
				carry = 0;
			}
			p->next = new ListNode(tmp);
			p = p->next;
			l2 = l2->next;
		}
			 
		if (carry == 1) {
			p->next = new ListNode(1);
		}	

		// delete the first 0 element
		p = output; 
		output = output->next;
		delete p;

		return output;
	}
};

int main()
{
	Solution sol;
	ListNode *l1 = new ListNode(1);
	ListNode *p = l1;
	p->next = new ListNode(8);
	ListNode *l2 = new ListNode(0);

	dump(l1); dump(l2);

	ListNode *ret = sol.addTwoNumbers(l1, l2);

	dump(ret);

	return 0;
}
