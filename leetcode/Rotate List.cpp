
 /*
 注意，题目中 rotate the list to the right by k places， 
 实际要求如果k 大于元素个数，继续循环
 见 19 行处理过程
 */
class Solution {
public:
	ListNode *rotateRight(ListNode *head, int k) {
		ListNode *prev; 
		ListNode *cur = head, *tail = head;

		if (k == 0) 
			return head;

		//  cur has k elements befor tail
		for (int i = 1; i < k; i++) {
			if (tail->next == NULL) {
				tail = head;   // for test case input: {1,2}, 3
						// out put: {2,1}
			}
			else {
				tail = tail->next;
			}
		}

		if (tail == NULL) 
			return head;

		ListNode dummy(-1);
		dummy.next = head;
		head = &dummy;
		prev = head; // prev  is befor cur

		// find the List tail, setup the prev, cur, tail pointers.
		while (tail->next != NULL) {
			prev = prev->next;
			cur = cur->next;
			tail = tail->next;
		}

		// we find it!
		//
		if (head == prev) {

		}
		else {
			tail->next = head->next;
			head->next = cur;
			prev->next = NULL;
		}

		// jump over dummy
		head = head->next;

		return head;
	} 
};
