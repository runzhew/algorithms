#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
	vector<vector<string> > solveNQueens(int n) {
		vector<vector<bool> > matrix(n, vector<bool>(n, false));
		dfs(0, matrix, n);
		return result;
	}

	void dump(vector<vector<string> > &matrix) const
	{
		int i, j;
		for (i = 0; i < matrix.size(); i++) {
			for (j = 0; j < matrix[i].size(); j++) {
				cout << matrix[i][j] << endl;
			}
			cout << endl;
		}
	}
private:

	vector<vector<string> > result;
	vector<string> tmp;

	void dfs(int x, vector<vector<bool> > &matrix, int n) {
		if (x == n) {
			build_solution(matrix);
			return ;
		}

		for (int i = 0; i < n; i++) {
			if (check(x, i, matrix, n) == true) {
				matrix[x][i] = true;
				//cout << "(" << x << " , " << i << ") is true, \
					next line: " << x+1 << endl;
				dfs(x+1, matrix, n);

				// IMPORTANT
				// after the next DFS return, we should set 
				// the matrix[x][i] to be false;
				matrix[x][i] = false;
			}
		}
	}

	void build_solution(vector<vector<bool> > &matrix)
	{
		string str;
		tmp.clear();
		for (int i = 0; i < matrix.size(); i++) {	
			str.clear();
			for (int j = 0; j < matrix.size(); j++) {
				if (matrix[i][j] == false) {
					str += '.';
				}
				else {
					str += 'Q';
				}
			}
			tmp.push_back(str);
		}
		result.push_back(tmp);
	}


	const bool check(int x, int y, vector<vector<bool> > &matrix, int n) const {
		int i, j;


		// bottom to top
		for (i = x; i >= 0; i--) {
			if (matrix[i][y] == true) {
				return false;
			}
		}
		// left to top right 
		for (i = x, j = y; (i >= 0) && (j < n); i--, j++) {
			if (matrix[i][j] == true) {
				return false;
			}
		}
		// right to top left
		for (i = x, j = y; (i >= 0) && (j >= 0); i--, j--) {
			if (matrix[i][j] == true) {
				return false;
			}
		}

		// check whether the x/y is too big that escape the "for" cases 
		if (x < n && x >= 0 && y <n && y >= 0) {
			return true;
		}
		else {
			return false;
		}
	}
};


int main()
{
	Solution sol;
	vector<vector<string> > ret = sol.solveNQueens(5);
	sol.dump(ret);

	return 0;
}
