class Solution {
public:
	bool searchMatrix(vector<vector<int> > &matrix, int target) {

		// search on first col
		int row = matrix.size();
		int col = matrix[0].size();

		int l = 0, r = row-1;
		int mid = (l+r)/2;

		while (l <= r) {
			mid = (l+r)/2;

			if (matrix[mid][0] < target) {
				// for matrix[mid][0] < target
				// we must check whether it is possible that the target 
				// is in this line. 
				// but for matrix[mid][0] > target, we don't need to check 
				// because matrix[mid][0] is the smallest number in this line 
				if (matrix[mid][col-1] >= target) {
					for (int i = 0; i < col; i++) {
						if (matrix[mid][i] == target)
							return true;
					}
					return false;
				}
				else {
					l = mid + 1;
				}
			}
			else if (matrix[mid][0] > target) {
				// matrix[mid][0] > target, we do not need to check 
				// just r = mid - 1
				r = mid - 1;
			}
			else {
				return true;
			}
		}	

		return false;
	}
}
