#include <iostream>
#include <sstream>
using namespace std;
/**
 * Definition for binary tree
 */
 struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 };


class Solution {
public:
    int sumNumbers(TreeNode *root) {
        
        int sum = 0;
        dfs(root, 0, sum);
        
        return sum;
        
    }
    
    void dfs(TreeNode *root, int t, int &sum)
    {
        if (root == NULL) {
            return;
        }
        
        if (root->left == NULL && root->right == NULL) {
            t += root->val;
            sum += t;
            return ;
        }
        
        t += root->val;
        
        dfs(root->left, t*10, sum);
        dfs(root->right, t*10, sum);
    }
};



class Solution {
public:
	int sumNumbers(TreeNode *root) {
		sum = 0;
		s = "";

		if (root == NULL) {
			return 0;
		}

		travel(root, s);

		return sum;
	}

	void travel(TreeNode * root, string s)
	{
		stringstream ss;

		if (root == NULL) {
			return ;
		}

		ss << root->val; // convert int to stringstream
		s = s + ss.str(); // ss.str() return a string type

		if (root->left == NULL && root->right == NULL) {
			int t; 
			ss.str(s);
			ss >> t;
			sum += t;
			return;
		}

		travel(root->left, s);
		travel(root->right, s);
	}
private:
	int sum;
	string s;
};


int main()
{
	stringstream ss;
	string  str = "01234";
	int i;
	ss.str(str);
	ss >> i;

	cout << "i = " << i << endl;
	return 0;
}
