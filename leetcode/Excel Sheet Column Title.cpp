#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

class Solution {
public:
	string convertToTitle(int n) {
		char dict[] = {'Z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
				'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

		string result;
		
		int ret;

		while (n > 26) {
			ret = n % 26;
			// the if case just for deal with carry
			// when it comes with carry numbers, such as 52 = 26 * 2;
			// when ret == 0, we should give dic[ret] = 'Z', and Decrease n--
			if (ret == 0) {
				n--;
			}
			n = n / 26;
			result = result + dict[ret];
		}
		result = result + dict[n];
		reverse(result.begin(), result.end());

		return result;
	}
};


int main()
{
	Solution sol;
	cout << sol.convertToTitle(26) << endl;
	cout << sol.convertToTitle(29) << endl;
	cout << sol.convertToTitle(52) << endl;
	cout << sol.convertToTitle(1227) << endl;

	return 0;
}
