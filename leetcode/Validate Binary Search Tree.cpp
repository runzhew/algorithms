/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	bool isValidBST(TreeNode *root) {
		if (root == NULL)
			return true;

		return check(root);
	}

	bool check(TreeNode *root)
	{
		if (root == NULL) {
			return true;
		}

		int right_min_val;
		int left_max_val;

		if (root->right != NULL) {
			right_min_val = root->right->val;
			get_right_subtree_min_val(root->right, &right_min_val);
			if (root->val >= right_min_val)	
				return false;
		}

		if (root->left != NULL) {
			left_max_val = root->left->val;
			get_left_subtree_max_val(root->left, &left_max_val);
			if (root->val <= left_max_val) {
				return false;
			}
		}

		return check(root->left) && check(root->right);
	}

	void get_left_subtree_max_val(TreeNode *t, int * max)
	{
		if (t == NULL) 
			return; 
		if (t->val > *max) 
			*max = t->val;

		get_left_subtree_max_val(t->left, max);
		get_left_subtree_max_val(t->right, max);
	}

	void get_right_subtree_min_val(TreeNode *t, int *min)
	{
		if (t == NULL)
			return;
		if (t->val < *min)
			*min = t->val;

		get_right_subtree_min_val(t->left, min);
		get_right_subtree_min_val(t->right, min);
	}
};
