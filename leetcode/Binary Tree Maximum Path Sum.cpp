/*
http://blog.csdn.net/fightforyourdream/article/details/16894069
找最大值还是不太会
*/

class Solution {
public:
	int maxPathSum(TreeNode *root) {
		if (root == NULL) {
			return 0;
		}
		int res = INT_MIN;
		traval(root, res);
		return res;
	}

	int traval(TreeNode *root, int &res)
	{
		if (root == NULL) {
			return 0;
		}
        
        int left = traval(root->left, res);
        int right = traval(root->right, res);
        
        int wan = left + root->val + right;
        
        int bu_wan_left = root->val + left;
        int bu_wan_right = root->val + right;
        
        int t = max(root->val, max(bu_wan_left, bu_wan_right));
        
        res = max(res, max(wan, t));
        
		return t;
	}

	int max(int a, int b)
	{
	    return a > b ? a : b;
	}
};
