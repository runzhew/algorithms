class Solution {
public:
    int canCompleteCircuit(vector<int> &gas, vector<int> &cost) {
        int n = gas.size();
        
        int diff[n];
        int sum = 0;
        
        for (int i = 0; i < n; i++) {
            diff[i] = gas[i] - cost[i];
            sum += diff[i];
        }
        
        if (sum < 0) {
            return -1;
        }
        
        int startNode = 0, tempSum = 0;
        
        for (int i = 0; i < n; i++) {
            tempSum += diff[i];
            if (tempSum < 0) {
                tempSum = 0;
                startNode = i + 1;
            }
        }
        
        return startNode;
    }
};


class Solution {
public:
	int canCompleteCircuit(vector<int> &gas, vector<int> &cost) {

		int i, k = 0;
		for (i = 0; i < gas.size();) {
			if (isvalid(gas, cost, k)) {
				return i;
			}
			i = k;
		}

		return -1;

	}

	bool isvalid(vector<int> &gas, vector<int> &cost, int &start)
	{
		int sum = 0;
		int n = gas.size();
		int i;
		for (i = start; i < (n + start); i++) {
			sum += gas[i%n] - cost[i%n];
			if (sum < 0) {
				start = i + 1; // 设定 下一个开始的位置
				return false;
			}
		}
		return true;
	}
};
