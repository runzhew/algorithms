#include <iostream>

using namespace std;

class Solution {
public:
	int uniquePaths(int m, int n) {

		count = 0;
		dfs(0, 0, m, n);

		return count;
	}
private:
	int count;

	void dfs(int x, int y, int m, int n) {

		//cout << "(" << x << " , " << y << ")" << endl;

		if ((x >= m) || (y >= n)) {
			return ;
		}

		// IMPORTANT 
		//
		// 	Pay attention to the 
		// 	(x == m) or (x+1 == m)
		if ((x+1 == m) && (y+1 == n)) {
			count++;
			return ;
		}

		dfs(x+1, y, m, n);
		dfs(x, y+1, m, n);
	}
};

int main(void)
{
	Solution sol;

	cout << sol.uniquePaths(23, 12) << endl;

	return 0;
}
