/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
	ListNode *mergeKLists(vector<ListNode *> &lists) {
	    
	    if (lists.size() == 0) {
	        return NULL;
	    }
        return split(lists, 0, lists.size()-1);
	}

private:

    ListNode *split(vector<ListNode *> &lists, int left, int right)
    {
        if (left < right) {
            int m = (left+right)/2;
            return mergeTwoLists(split(lists, left, m), split(lists, m+1, right));
        }
        
        return lists[left];
    }
    
    ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
        
        ListNode dummy(-1);
        ListNode *p = &dummy;
        
        while (l1 != NULL && l2 != NULL) {
            if (l1->val < l2->val) {
                p->next = l1;
                l1 = l1->next;
            }
            else {
                p->next = l2;
                l2 = l2->next;
            }
            
            p = p->next;
        }
        
        if (l1 != NULL) {
            p->next = l1;
        }
        
        if (l2 != NULL) {
            p->next = l2;
        }
        
        return dummy.next;
        
    }
};
