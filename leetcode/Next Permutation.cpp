#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
	void nextPermutation(vector<int> &num) {
		int first_not_increase = -1;
		int less_than = -1;
		int n = num.size();

		// find the first_not_increase 
		for (int i = n-1; i > 0; i--) {
			if (num[i-1] >= num[i]) {
				;
			}
			else {
				first_not_increase = i-1;
				break;
			}
		}

		// not found, then it must be the first one
		if (first_not_increase < 0) {
			;
		}
		else {
			// find less_than 
			for (int i = n-1; i >= 0; i--) {
				if (num[i] > num[first_not_increase]) {
					less_than = i;
					break;
				}
			}
			if (less_than >= 0) {
				// exchange
				int tmp = num[first_not_increase];
				num[first_not_increase] = num[less_than];
				num[less_than] = tmp;
			}
		}


		// reverse
		int start = first_not_increase + 1; 
		int end = n-1;

		while (start < end) {
			int t = num[start];
			num[start] = num[end];
			num[end] = t;

			start++;
			end--;
		}

	}
};

int main() 
{
	Solution sol;
	int m[3] = {3, 2, 1};
	vector<int> num(m, m+3);

	sol.nextPermutation(num);

	for (int i = 0; i < num.size(); i++) {
		cout << num[i] << " " ;
	}
	cout << endl;
	return 0;
}
