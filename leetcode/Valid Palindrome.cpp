#include <iostream>

using namespace std;

class Solution {
	public:
		bool isPalindrome(string s) {
			if (s.empty())
				return true;

			int head = 0;
			int rear = s.size() - 1;

			for (int i = 0; i < s.size(); i++) {
				if (isChar(s[i])) {
				    s[i] = tolower(s[i]);
				}  

			}

			while (head < rear) {

				while ((head < rear) && !isChar(s[head])) {
					head++;
				}	
				while ((head < rear) && !isChar(s[rear]))  {
					rear--;
				}

				if (s[head] != s[rear]) {
					break;
				}
				else {
					head++;
					rear--;
				}
			}

			if (head < rear) {
				return false;
			}
			else 
				return true;
		}
	private:
		bool isChar(char ch)
		{
			if (ch >= 'a' && ch <= 'z') return true;
			if (ch >= 'A' && ch <= 'Z') return true;
			if (ch >= '0' && ch <= '9') return true;

			return false;
		}
};

int main()
{
	Solution sol;
	sol.isPalindrome("aA");
	return 0;
}
