#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    int minimumTotal(vector<vector<int> > &triangle) {
        
        int h = triangle.size()-1;
        
        for (int i = h-1; i >= 0; i--) {
            
            int len = triangle[h].size();
            
            for (int j = 0; j < len; j++) {
                triangle[i][j] += min(triangle[i+1][j], triangle[i+1][j+1]);
            }
        }
        
        return triangle[0][0];
        
    }
};


class Solution2 {
public:
	int minimumTotal(vector<vector<int> > &triangle) {

		int row = triangle.size();

		vector<int> sum;

		for (int i = 0; i < triangle[row-1].size(); i++) {
			sum.push_back(triangle[row-1][i]);
		}

		for (int i = row-2; i >= 0; i--) {
			for (int j = 0; j < triangle[i].size(); j++) {
				sum[j] =  ((sum[j] < sum[j+1]) ? sum[j] : sum[j+1]) + triangle[i][j];
			}
		}

		return sum[0];
	}
};

int main()
{
	Solution sol;
	vector<int> t;
	vector<vector<int> > triangle;

	t.push_back(2);
	triangle.push_back(t);
	t.clear();

	t.push_back(3);
	t.push_back(3);
	triangle.push_back(t);
	t.clear();


	t.push_back(6);
	t.push_back(5);
	t.push_back(7);
	triangle.push_back(t);
	t.clear();

	t.push_back(4);
	t.push_back(1);
	t.push_back(8);
	t.push_back(3);
	triangle.push_back(t);
	t.clear();

	cout << sol.minimumTotal(triangle) << endl;
	return 0;
}




