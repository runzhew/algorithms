#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
	vector<vector<int> > permute(vector<int> &num) {
		int n = 1;
		for (int i = 1; i <= num.size(); i++) {
			n = n * i;
		}

		sort(num.begin(), num.end());

		vector<vector<int> > result;

		for (int i = 0; i < n; i++) {
			result.push_back(num);
			nextPermutation(num);
		}

		return result;

	}

	void nextPermutation(vector<int> &num) {
		int first_not_increase = -1;
		int less_than = -1;
		int n = num.size();

		// find the first_not_increase 
		for (int i = n-1; i > 0; i--) {
			if (num[i-1] >= num[i]) {
				;
			}
			else {
				first_not_increase = i-1;
				break;
			}
		}

		// not found, then it must be the first one
		if (first_not_increase < 0) {
			;
		}
		else {
			// find less_than 
			for (int i = n-1; i >= 0; i--) {
				if (num[i] > num[first_not_increase]) {
					less_than = i;
					break;
				}
			}
			if (less_than >= 0) {
				// exchange
				int tmp = num[first_not_increase];
				num[first_not_increase] = num[less_than];
				num[less_than] = tmp;
			}
		}


		// reverse
		int start = first_not_increase + 1; 
		int end = n-1;

		while (start < end) {
			int t = num[start];
			num[start] = num[end];
			num[end] = t;

			start++;
			end--;
		}

	}
};

int main()
{
	Solution sol;
	vector<vector<int> > ret;
	int t[] = {1,1,4,7};
	vector<int> num(t, t+4);

	ret = sol.permute(num);

	for (int i = 0; i < ret.size(); i++) {
		for (int j = 0; j < ret[i].size(); j++) {
			cout << ret[i][j] << " " ;
		}
		cout << endl;
	}
	return 0;
}
