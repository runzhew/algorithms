#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;


class Solution {
public:
    vector<vector<int> > combinationSum2(vector<int> &num, int target) {
        
        vector<vector<int> > result;
        vector<int> tmp;
        
        sort(num.begin(), num.end());
        
        dfs(0, num, target, result, tmp);
        
        return result;
    }
    
    void dfs(int start, vector<int> &num, int target, 
            vector<vector<int> > &result, vector<int> &tmp) {

        if (target < 0) {
            return ;
        }        
        
        if (target == 0) {
            result.push_back(tmp);
            return ;
        }
        
        for (int i = start; i < num.size(); i++) {
            if (i > start && num[i] == num[i-1]) {  // i > start 这个条件非常重要！！
                continue;
            }
            
            tmp.push_back(num[i]);
            dfs(i+1, num, target-num[i], result, tmp);
            tmp.pop_back();
        }
    }
};

int main()
{
	vector<int> num;
	num.push_back(1);
	num.push_back(2);

	Solution sol;
	vector<vector<int> > res = sol.combinationSum2(num, 4);

	return 0;
}
