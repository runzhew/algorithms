/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

class Solution {
public:
    ListNode *deleteDuplicates(ListNode *head) {
        if (head == NULL) {
            return NULL;
        }
        ListNode dummy(-1);
        dummy.next = head;
        ListNode * prev = &dummy, *cur = head, *next = head->next;
        
        while (next != NULL) {
            if (cur->val == next->val) {
                while (next != NULL && cur->val == next->val) {
                    next = next->next;
                }
                prev->next = next;
                cur = next;
                if (next != NULL) {
                    next = next->next;
                }
            }
            else {
                prev = cur;
                cur = next;
                next = next->next;
            }
        }
        
        return dummy.next;
        
    }
};


class Solution {
public:
	ListNode *deleteDuplicates(ListNode *head) {

		if (head == NULL) 
			return head;

		ListNode *pre_slow = head;
		ListNode *slow = head;
		ListNode *fast = head->next;

		while (fast != NULL) {
			while (fast != NULL && fast->val == slow->val) {
				fast = fast->next;
			}
			if (fast == NULL && slow != head) {
				pre_slow->next = NULL;
				break;
			}
			else if (fast == NULL && slow == head){
				head = NULL;
				break;
			}

			if (slow->next == fast) {
				// distinct number
				pre_slow = slow;
				slow = slow->next;
				fast = fast->next;
			}
			else {
				// duplicate is not start from head
				// 1->2->2->3
				if (slow != head) {
					pre_slow->next = fast;
					slow = fast;
					fast = fast->next;
				}
				//duplicate is from head
				// 1->1->1->2
				else {
					pre_slow = fast;
					head = fast;
					slow = fast;
					fast = fast->next;
				}
			}
		}
		return head;
	}
};
