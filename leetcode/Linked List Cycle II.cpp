/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
	ListNode *detectCycle(ListNode *head) {
		ListNode *slow = head, *fast = head;
		ListNode *slow2 = head;

		if (head == NULL) {
			return NULL;
		}

		while (fast != NULL && fast->next != NULL) {
			slow = slow->next;
			fast = fast->next->next;

			if (slow == fast) {
				while (slow != slow2) {
					slow = slow->next;
					slow2 = slow2->next;
				}
				return slow;
			}
		}
	}
};
