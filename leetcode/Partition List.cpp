
/*
一个很好的思路： 遇到将链表分成两部分的题目，新建两个dummy的头，
把符合条件的放到各自对的的dummy为头的链表中去。

一般 dummy 这样使用

ListNode less_dummy(-1);          // 新建
ListNode *cur_less = &less_dummy; // 获取指针

这样就保证了后面指针使用是一致性

*/
class Solution {
public:
	ListNode *partition(ListNode *head, int x) {
		ListNode less_dummy(-1);
		ListNode grate_dummy(-1);

		ListNode *cur_less = &less_dummy;
		ListNode *cur_grate = &grate_dummy;
		while (head != NULL) {
			if (head->val < x) {
				cur_less->next = head;
				cur_less = cur_less->next;
			}
			else {
				cur_grate->next = head;
				cur_grate = cur_grate->next;
			}
			head = head->next;
		}

		cur_less->next = grate_dummy.next;
		cur_grate->next = NULL;

		return less_dummy.next;
	}
};
