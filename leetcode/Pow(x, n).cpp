#include <iostream>

using namespace std;

class Solution {
public:

	Solution():recursive_count(0), liner_count(0) {}

	double pow(double x, int n) {

		if (n == 0) 
			return 1;

		if (n < 0)
			return 1/power(x, -n);

		return power(x, n);
	}
	double power(double x, int n)
	{
		if ( n == 0 )
			return 1;

		double v = power(x, n/2);
		if (n % 2 == 0) {
			recursive_count++;
			return v * v;
		}
		recursive_count++;
		return v * v * x;
	}

	double power_liner(double x, int m) 
	{
		int n;
		double result = 1;

		if (m < 0) 
			n = -m;
		else 
			n = m;
		
		for (int i = 1; i <= n; i++) {
			result = result * x;
			liner_count++;
		}
		return result; 
	}

	void dump()
	{
		cout << "recursive_count = " << recursive_count << endl;
		cout << "liner_count = " << liner_count << endl;
	}

private:
	int recursive_count;
	int liner_count;
};


int main(void)
{
	Solution sol;

	cout << "5; 102 times: " << sol.power(5, 102) << endl;
	cout << "5; 102 times: " << sol.power_liner(5, 102) << endl;

	sol.dump();

	return 0;
}
