#include <iostream>

using namespace std;

class Solution {
public:
	int singleNumber(int A[], int n) {
        
		int array[32];
		// set, initialize to 0
		for (int i = 0; i < 32; i++) {
			array[i] = 0;
		}

		// do all the binary add
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < 32; j++) {
				array[j] += (A[i] & 1);
				A[i] = A[i] >> 1;
			}
		}

		// mod 3
		for (int i = 0; i < 32; i++) {
			array[i] = array[i] % 3;
		}


		// caculate the num from binary 0000111
		int sum = 0;
		for (int i = 0; i < 32; i++) {
			if (array[i] == 1) {
				sum = sum + (1 << i);
			}
		}
		return sum;
	}
};


int main()
{
	Solution sol;
	int a[] = {12, 1, 12, 3, 12, 1, 1, 2, 3, 3};
	cout << sol.singleNumber(a, 10);
	return 0;
}
