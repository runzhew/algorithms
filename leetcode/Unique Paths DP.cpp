#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
	int uniquePaths(int m, int n) {
		
		vector<vector<int> > matrix(m, vector<int>(n, 0));		 
		int i, j;

		for (i = 0; i < m; i++) {
			matrix[i][0] = 1;
		}
		for (i = 0; i < n; i++) {
			matrix[0][i] = 1;
		}

		for (i = 1; i < m; i++) {
			for (j = 1; j < n; j++) {
				matrix[i][j] = matrix[i][j-1] + matrix[i-1][j];
			}
		}

		return matrix[m-1][n-1];
	}
};

int main(void)
{
	Solution sol;

	cout << sol.uniquePaths(23, 12) << endl;

	return 0;
}
