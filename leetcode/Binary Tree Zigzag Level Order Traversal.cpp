/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
	public:
		vector<vector<int> > zigzagLevelOrder(TreeNode *root) {

			stack<TreeNode* > parent_level;
			stack<TreeNode *> child_level;
			vector<vector<int> > result;
			vector<int> level;
			bool left_2_right = true;

			if (root == NULL)
				return result;

			parent_level.push(root);

			while (!parent_level.empty()) {

				while (!parent_level.empty()) {
					TreeNode * t = parent_level.top();
					parent_level.pop();

					level.push_back(t->val);

					if (left_2_right == true) {
						if (t->left != NULL) 
							child_level.push(t->left);
						if (t->right != NULL)
							child_level.push(t->right);
					}
					else {
						if (t->right != NULL)
							child_level.push(t->right);
						if (t->left != NULL) 
							child_level.push(t->left);
					}
				}

				result.push_back(level);

				if (left_2_right == true)
					left_2_right = false;
				else 
					left_2_right = true;

				level.clear();

				parent_level.swap(child_level);
			}
			return result;
		}

};
