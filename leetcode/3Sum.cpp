#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
	vector<vector<int> > threeSum(vector<int> &num) {
		vector<vector<int> > result;
		if (num.size() < 3)
			return result;

		sort(num.begin(), num.end(), less<int>());

		for (int i = 0; i < num.size(); i++)
			cout << num[i] << ", ";
		cout << endl;

		int n = num.size(), i;
		for (i = 0; i < n-2; i++) {
			if (i > 0 && num[i] == num[i - 1])continue; // jump over the same number
			int target = 0 - num[i];
			cout << "i = " << i << endl;
			jiabi_2sum(num, result, i, target);
		}
		return result;
	}

	// 用夹逼原则，所以比暴力枚举稍微快了一点点
	void jiabi_2sum(vector<int> &num, vector<vector<int> > &result, int cur, int target)
	{
		int end = num.size() - 1;
		int start = cur + 1;

		while (start < end) {
			cout << "start = " << start << " , end = " << end << endl;
			int tmp = num[start] + num[end];
			if ( tmp == target) {
				result.push_back(vector < int > {num[cur], num[start], num[end]});
				start++;
				end--;
				// only in this if case, we can 左边夹逼 and 右边夹逼
				while (num[start] == num[start - 1]) start++;
				while (num[end] == num[end + 1]) end--;
			}
			else if (tmp < target) { // we can not 夹逼 here
				start++;
			}
			else if (tmp > target) { //  同理，we cann't 夹逼 here
				end--;
			}
		}

	}
};

int main()
{
	vector<int> num({-1, 0, 1, 2, -1, -4, 2, 5, -3, 7, -6, 8, -1, 9, -7});

	Solution sol;
	vector<vector<int> > re = sol.threeSum(num);

	for (int i = 0; i < re.size(); i++) {
		for (int j = 0; j < re[i].size(); j++) {
			cout << re[i][j] << ", ";
		}
		cout << endl;
	}
	system("pause");
	return 0;
}
