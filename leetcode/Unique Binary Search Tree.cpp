#include <iostream>

class Solution {
public:
	int numTrees(int n) {

		int array[n+1];
		int i, j, left, right;
		int sum, tmp;

		array[0] = 1;
		array[1] = 1;
		array[2] = 2;

		for (i = 3; i <= n; i++) {
			sum = 0;
			for (j = 1; j <= i; j++) {
				left = j - 1;
				right = i - j;

				tmp = array[left] * array[right];
				sum += tmp;
			}
			array[i] = sum;
		}

		return array[n];
	}
};

int main()
{
	Solution sol;
	std::cout << sol.numTrees(4);

	return 0;
}
