
// a very fast solution
class Solution {
public:
    vector<vector<int> > pathSum(TreeNode *root, int sum) {
        
        vector<vector<int> > result;
        vector<int> tmp;
        
        dfs(root, result, tmp, sum);
        
        return result;
        
    }
    
    void dfs(TreeNode *root, vector<vector<int> > &result, vector<int> &tmp, int sum)
    {
        if (root == NULL) {
            return ;
        }
        sum -= root->val;
        tmp.push_back(root->val);
        
        if (root->left == NULL && root->right == NULL && sum == 0) {
            result.push_back(tmp);
            tmp.pop_back();
            return ;
        }
        
        dfs(root->left, result, tmp, sum);
        dfs(root->right, result, tmp, sum);
        tmp.pop_back();
    }
};

// a very slow solution
class Solution {
public:
    vector<vector<int> > pathSum(TreeNode *root, int sum) {
        
        vector<vector<int> > result;
        vector<int> tmp;
        
        dfs(root, result, tmp, sum);
        
        return result;
        
    }
    
    void dfs(TreeNode *root, vector<vector<int> > &result, vector<int> tmp, int sum)
    {
        if (root == NULL) {
            return ;
        }
        sum -= root->val;
        tmp.push_back(root->val);
        
        if (root->left == NULL && root->right == NULL && sum == 0) {
            result.push_back(tmp);
            return ;
        }
        
        dfs(root->left, result, tmp, sum);
        dfs(root->right, result, tmp, sum);
    }
};
 
 
 
 
 
class Solution {
public:
	vector<vector<int>> pathSum(TreeNode *root, int sum) {
		int tmp_sum = 0;
		given_sum = sum;
		vector<int> v;

		if (root == NULL)
			return result;

		traveler(root, tmp_sum, v);

		return result;
	}

private:
	int given_sum;
	vector<vector<int>> result;

// 注释中的这个写法是对的，非注释的写法有错，不知道为什么

//	void traveler(TreeNode *root, int tmp_sum, vector<int> v)
//	{
//		if (root->left == NULL && root->right == NULL) {
//			if (tmp_sum + root->val  == given_sum) {
//				v.push_back(root->val);
//				result.push_back(v);
//				return ;
//			}
//			return ;
//		}
//
//		tmp_sum += root->val;
//		v.push_back(root->val);
//		
//
//		if (root->left != NULL) {
//			traveler(root->left, tmp_sum, v); 
//		}
//		if (root->right != NULL) {
//			traveler(root->right, tmp_sum, v);
//		}
//
//	}
	void traveler(TreeNode *root, int tmp_sum, vector<int> &v)
	{
		v.push_back(root->val);

		if (root->left == NULL && root->right == NULL) {
			if (tmp_sum + root->val  == given_sum) {
				result.push_back(v);
				v.pop_back();
				return ;
			}
			v.pop_back();
			return ;
		}

		tmp_sum += root->val;

		if (root->left != NULL) {
			traveler(root->left, tmp_sum, v); 
		}
		if (root->right != NULL) {
			traveler(root->right, tmp_sum, v);
		}

		v.pop_back();

	}
};
