
// 这个方法可以AC， 但是pdf上有“更高级” 的分治法。
// http://www.geeksforgeeks.org/find-a-peak-in-a-given-array/

class Solution {
public:
    int findPeakElement(const vector<int> &num) {
        if (num.size() == 1) {
            return 0;
        }
        int n = num.size();
        // index 0
        if (num[0] > num[1]) {
            return 0;
        }
        // index n
        if (num[n-1] > num[n-2]) {
            return n-1;
        }
        
        // from 0 to n
        
        for (int i = 1; i < n-1; i++) {
            if (num[i] > num[i-1] && num[i] > num[i+1])  {
                return i;
            }
        }
        
    }
};
