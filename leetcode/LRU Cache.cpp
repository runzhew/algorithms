class LRUCache{
public:
	LRUCache(int capacity) {
		this->capacity = capacity;
	}

	int get(int key) {

		if (mapper.find(key) == mapper.end()) {
			return -1;
		}

		// update cache
		list<node>::iterator it = mapper[key];
		int v = it->value;
		cache.erase(it);
		struct node nd(key, v);
		cache.push_front(nd);
		mapper[key] = cache.begin();
		return v;
	}

	void set(int key, int value) {

		struct node nd(key, value);
		// if key-value does not exist
		if (mapper.find(key) == mapper.end()) {
			if (cache.size() < this->capacity) {
				cache.push_front(nd);
				mapper[key] = cache.begin();
			}
			else {
				int k = cache.back().key;
				cache.pop_back();
				mapper.erase(k);
				cache.push_front(nd);
				mapper[key] = cache.begin();
			}
		}
		else {
			list<node>::iterator it = mapper[key];
			cache.erase(it);
			cache.push_front(nd);
			mapper[key] = cache.begin();
		}
	}
private:
	struct node {
		int key;
		int value;
		node(int k, int v): key(k), value(v){}
	};

	list<struct node> cache;
	map<int, list<struct node>::iterator> mapper;
	int scapacity;
};
