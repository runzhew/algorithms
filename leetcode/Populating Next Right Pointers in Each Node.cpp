/**
 * Definition for binary tree with next pointer.
 * struct TreeLinkNode {
 *  int val;
 *  TreeLinkNode *left, *right, *next;
 *  TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
 * };
 */
class Solution {
public:
    /**
    首先想到的方法是 广度优先遍历，但是需要O(n)的空间复杂度，
    要想获得常量O(1)的空间复杂度， 只需要用2个指针去模拟 BFS
    **/
    void connect(TreeLinkNode *root) {
        if (root == NULL)
            return;

        TreeLinkNode *prev_node = NULL;
        TreeLinkNode *next_level_head = NULL;

        while(root) {
            prev_node = NULL;
            next_level_head = NULL;
            for (; root; root = root->next) {
                if (next_level_head == NULL) { // 在这里更新next的值
                    next_level_head = root->left ? root->left : root->right;
                }

                if (root->left) {
                    if (prev_node) {
                        prev_node->next = root->left;
                    }
                    prev_node = root->left;
                }

                if (root->right) {
                    if (prev_node) {
                        prev_node->next = root->right;
                    }
                    prev_node = root->right;
                }
            }
            root = next_level_head;
        }
    }
};
