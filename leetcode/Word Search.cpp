#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
	bool exist(vector<vector<char> > &board, string word) {

		m = board.size();
		n = board[0].size();

		word_len = word.size();

		vector<vector<bool> > visited(m, vector<bool>(n, false));

		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (board[i][j] == word[0]) {
					//cout << "i = " << i << ", j = " << j << endl;
					if (dfs(board, word, i, j, 0, visited)) {
						return true;
					}
				}
			}
		}
		return false;
	}

private:
	int word_len;
	int m, n;

	bool dfs(vector<vector<char> > &board, string &word, 
		 int i, int j, int len, vector<vector<bool> > &visited)
	{
		bool ret = false;

		if (len == word_len)
			return true;
		
		if (i < 0 || j < 0 || i >= board.size() || j >= board[0].size())
			return false;

		if (visited[i][j]) 
			return false;

		if (board[i][j] != word[len]) {
			return false;
		}
		else {
			visited[i][j] = true;
			ret = dfs(board, word, i-1, j, len+1, visited) ||
				dfs(board, word, i+1, j, len+1, visited) ||
				dfs(board, word, i, j-1, len+1, visited) ||
				dfs(board, word, i, j+1, len+1, visited);
			visited[i][j] = false;
			return ret;
	 	}
	}
};


int main()
{
	Solution sol;
	vector<char> t;
	vector<vector<char> > board;
	t.push_back('C');
	t.push_back('A');
	t.push_back('A');

	board.push_back(t);

	t.clear();

	t.push_back('A');
	t.push_back('A');
	t.push_back('A');
	board.push_back(t);

	t.clear();

	t.push_back('B');
	t.push_back('C');
	t.push_back('D');
	board.push_back(t);

	if (sol.exist(board, "AAB") == true) {
		cout << "true" << endl;
	}
	else {
		cout << "false" << endl;
	}

	return 0;
}
