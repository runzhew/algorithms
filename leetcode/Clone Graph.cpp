/**
 * Definition for undirected graph.
 * struct UndirectedGraphNode {
 *     int label;
 *     vector<UndirectedGraphNode *> neighbors;
 *     UndirectedGraphNode(int x) : label(x) {};
 * };
 */

//  Similar: Copy List with Random Pointer
 
// 这里比较有意思的地方是，基本的图的BFS遍历，需要设置一个vector visited记录
// 每个节点是否被访问。
// 而这个问题中，正好可以复用copy这个map，因为可以肯定的是，copy中已有映射的节点，肯定
// 已经被访问过，没有的肯定没有被访问过
class Solution {
public:
	UndirectedGraphNode *cloneGraph(UndirectedGraphNode *node) {
		if (node == NULL) 
			return NULL;

		map<UndirectedGraphNode *, UndirectedGraphNode *> copy;
		queue<UndirectedGraphNode *> q;

		q.push(node);
		UndirectedGraphNode *new_node = new UndirectedGraphNode(node->label);
		copy[node] = new_node;

		while (!q.empty()) {
			UndirectedGraphNode *cur = q.front();
			q.pop();

			for (int i = 0; i < cur->neighbors.size(); i++) {

				UndirectedGraphNode * neigh = cur->neighbors[i];

				// already copied
				if (copy.find(neigh) != copy.end()) {
					copy[cur]->neighbors.push_back(copy[neigh]);
				}
				else { // does not exist
					UndirectedGraphNode *neigh_copy = new UndirectedGraphNode(neigh->label);
					copy[neigh] = neigh_copy;
					copy[cur]->neighbors.push_back(neigh_copy);


					// if does not copied, it also means not visited before
					// 这里比较有意思的地方是，基本的图的BFS遍历，需要设置一个vector visited记录
					// 每个节点是否被访问。
					// 而这个问题中，正好可以复用copy这个map，因为可以肯定的是，copy中已有映射的节点，肯定
					// 已经被访问过，没有的肯定没有被访问过
					q.push(neigh);
				}
			}
		}

		return copy[node];

	}
};
