/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */

class Solution {
public:
	ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
		ListNode dummy(-1);
		ListNode *p = &dummy;

		while (l1 != NULL && l2 != NULL) {
			if (l1->val < l2->val) {
				p->next = l1;
				l1 = l1->next;
			}
			else {
				p->next = l2;
				l2 = l2->next;
			}
			p = p->next;
		}
		if (l1 != NULL) {
			p->next = l1;
		}
		if (l2 != NULL) {
			p->next = l2;
		}

		return dummy.next;
	}
};


class Solution {
public:
	ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
		
		if (l1 == NULL) 
			return l2;
		if (l2 == NULL)
			return l1;

		ListNode *head = new ListNode;
		ListNode * p = head;

		while (l1 != NULL && l2 != NULL) {
			if (l1->val < l2->val) {
				p->next = l1;
				p = l1;
				l1 = l1->next;
			}
			else {
				p->next = l2;
				p = l2;
				l2 = l2->next;
			}
		}
		// if l1 or l2 still have elements
		if (l1 != NULL)
			p->next = l1;
		if (l2 != NULL)
			p->next = l2;

		p = head;
		head = head->next;
		delete p;
		return head;

	}
};
