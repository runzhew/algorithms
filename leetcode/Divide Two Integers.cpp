#include <iostream>
#include <climits>

using namespace std;

class Solution {
public:
	int divide(int dividend, int divisor) {
		int sign = 1;
		if (dividend<0){ sign = -sign; }
		if (divisor<0){ sign = -sign; }

		if (divisor == 1) return dividend;
		if (divisor == -1 && dividend != INT_MIN) 
			// 这里加入 INT_MIN 纯粹是为了leetcode 测试用例最后一个case准备的
			// 最后一个case 的 dividend 是 -2147483648, 这是 int 型负数的最小值，但是不能把它直接返回
			// 因为 int 型正整数最大值为 2147483647
			//
			// 需记住 INT_MIN = -2147483648
			//       INT_MAX = 2147483647
			//
			// dividend = INT_MIN 的情况留到后面处理, 或者直接加一个if处理
			return -dividend;

		unsigned long long dvd = abs((long long)dividend);
		unsigned long long dvs = abs((long long)divisor);

		unsigned long c = 1;
		while (dvd>dvs){
			dvs = dvs << 1;
			c = c << 1;   // c = c << 1, 这样的计数方式比 step++ 好， 因为step 最后还要转化
		}

		int res = 0;
		while (dvd >= abs((long long)divisor)){
			while (dvd >= dvs){
				dvd -= dvs;
				res = res + c;
			}
			dvs = dvs >> 1;
			c = c >> 1;
		}

		int result = sign * res;
		if (result >= INT_MAX || result <= INT_MIN) {
			return INT_MAX;
		}

		return (int)result;
	}
};

class Solution_good {
public:
	int divide(int dividend, int divisor) {
		unsigned long dvd;
		unsigned long dvs;
		int sign = 1;
		
		if (dividend < 0) {
			sign *= -1;
			dvd = -dividend;
		}
		if (divisor < 0) {
			sign *= -1;
			dvs = -divisor;
		}

		if (dvd < dvs) return 0;

		unsigned long absDivisor = dvs;

		int step = 0;
		while (dvs < dvd) {
			dvs = dvs << 1;
			step++;
		}

		unsigned long result = 0;
		while (dvd >= absDivisor) {
			if (dvd >= dvs) {
				dvd = dvd - dvs;
				result += (unsigned long)(1 << step);
			}
			dvs = dvs >> 1;
			step--;
		}
		return result * sign;
	}
};


class Solution_Bad {
public:
	int divide(int dividend, int divisor) {

		int d1 = 1, d2 = 1;
		unsigned long dvd;
		unsigned long dvi;
		int total = 0;

		if (dividend < 0) {
			dvd = -dividend;
			d1 *= -1;
		}
		else {
			dvd = dividend;
		}

		if (divisor < 0) {
			dvi = -divisor;
			d2 *= -1;
		}
		else {
			dvi = divisor;
		}

		if (dvi == dvd)
			return 1;
		if (dvi > dvd)
			return INT_MAX;

		unsigned long dvi_saved = dvi;
		while (dvi < dvd) {

			int step = 0; //  2^n, step is n
			while (dvi < dvd) {
				dvi = dvi << 1;
				step++;
			}
			// turn back
			dvi = dvi >> 1;
			step--;
			// 
			dvd = dvd - dvi;
			dvi = dvi_saved;
			total = total + (1 << step);

			cout << "total = " << total <<endl;
			if (dvi == dvd) {
				total++;
				break;
			}
		}

		return (total * d1 * d2);
	}
};



int main()
{
	Solution_good  sol;

	cout << "-100/ 3 = " << endl << sol.divide(-100, 3) << endl;
	cout << "9 / -2 = " << endl << sol.divide(9, -2) << endl;
	cout << "-2147483648 / -1 = " << endl << sol.divide(-2147483648, -1) << endl;

	cout << INT_MAX << endl;
	return 0;
}
