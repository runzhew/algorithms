// 凡是遇到一维数组的问题，而且第一看看上去要用多次扫描，
// 时间复杂度可能是O(n!)
// 或者 O(n^2) 这种的问题。
// 肯定会有一种线性解法，而且通常都是设置头尾两个指针，向中间夹逼

class Solution {
public:
	int maxArea(vector<int> &height) {
		int start = 0;
		int end = height.size() - 1;

		int max = 0;

		while (start < end) {
			int h = (height[start] < height[end] ? height[start] : height[end]);
			int tmp = h * (end - start);

			max = max < tmp ? tmp : max;

			height[start] < height[end] ? ++start : --end;
		}
		return max;
	}
};
