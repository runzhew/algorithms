class Solution {
public:
	ListNode *deleteDuplicates(ListNode *head) {
		ListNode * pre, *cur;
		pre = head;
		if (head == NULL) 
			return head;
		cur = head->next;
		while (cur != NULL) {
			if (cur->val == pre->val) {
				cur = cur->next;
				pre->next = cur; // can't handle 1->1 
				// but if I add this line, the bug is fixed. 
				continue;
			}
			pre->next  = cur;
			pre = cur;
			cur = cur->next;
		}
		return head;
        
	}
};
