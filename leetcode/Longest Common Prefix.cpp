class Solution {
public:
    string longestCommonPrefix(vector<string> &strs) {
        string result;
        
        if (strs.size() == 0) {
            return "";
        }
        int pos = 0;
        for (pos = 0; pos < strs[0].size(); pos++) {
            for (int i = 1; i < strs.size(); i++) {
                if (strs[i][pos] != strs[0][pos]) {
                    return strs[0].substr(0, pos);
                }
            }
        }
        
        return strs[0].substr(0, pos);
    }
};


class Solution {                                                                                                                         
public:                                                                                                                                   
	string longestCommonPrefix(vector<string> &strs) { 
		string  result;                                                                                                          
		if (strs.size() == 0) 
			return ""; 
		int index = 0; 
		for (int k = 0; k < strs[0].size(); k++) {
			for (int i = 0; i < strs.size(); i++) {
				if (strs[i][index] != strs[0][index]) {
					return result;
				}
			}
			result += strs[0][index];
			index++;
		}

		return result;
	}
};  

