
// 有更简单的方法

class Solution {
public:
	int removeDuplicates(int A[], int n) {
		int i, index;
		int count = 1;
		// queue<int> q;
		// 用队列不合适，后续的23行while后面会出问题
		stack<int> st;

		for (i = 0; i < n-1; i++) {
			if (A[i] == A[i+1]) {
				count++;
				if (count > 2) {
					st.push(i+1);
				}
			}
			else {
				count = 1;
			}
		}

		int size = st.size();

		while (!st.empty()) {
			index = st.top();
			st.pop();
			
			for (i = index; i < n-1; i++) {
				A[i] = A[i+1];
			}
		}
		return n-size;
	}
};
