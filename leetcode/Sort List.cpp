/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
	ListNode *sortList(ListNode *head) {

		if (head == NULL || head->next == NULL) 
			return head;

        // 这里找到一个链表中点的方法很好，记住
		ListNode *fast = head, *slow = head;
		while (fast->next != NULL && fast->next->next != NULL) {
			fast = fast->next->next;
			slow = slow->next;
		}

		// split list 
		fast = slow;
		slow = slow->next;
		fast->next = NULL;

		// recursive 递归，好好理解
		ListNode *l1 = sortList(head);
		ListNode *l2 = sortList(slow);

		return mergeTwoLists(l1, l2);
	}

	ListNode *mergeTwoLists(ListNode *l1, ListNode *l2) {
		ListNode dummy(-1);
		ListNode *p = &dummy;

		while (l1 != NULL && l2 != NULL) {
			if (l1->val < l2->val) {
				p->next = l1;
				l1 = l1->next;
			}
			else {
				p->next = l2;
				l2 = l2->next;
			}
			p = p->next;
		}
		if (l1 != NULL) {
			p->next = l1;
		}
		if (l2 != NULL) {
			p->next = l2;
		}

		return dummy.next;
	}
};
