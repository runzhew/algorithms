class Solution {
public:
    bool hasCycle(ListNode *head) {

	    ListNode * first = head, *second = head;
	    bool has = false;

	    if (head == NULL) 
		    return false;

	    while(1) {
		    if (first->next == NULL) {
			    break;
		    }
		    else {
			    first = first->next;
		    }

		    if (second->next == NULL) {
			    break;
		    }
		    else if (second->next->next == NULL) {
			    break;
		    }
		    else {
			    second = second->next->next;
		    }

		    if (first == second) {
			    has = true;
			    return has;
		    }
	    }
	    return false;        
    }
};
