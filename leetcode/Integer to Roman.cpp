#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
	string intToRoman(int num) {
		string symbols[] = {"M", "CM", "D","CD", "C", "XC", "L", "XL", "X", "IX","V","IV","I"};
		int     values[] = {1000, 900, 500, 400, 100,  90,   50,  40,   10,   9,  5,   4,  1};
		
		string ret;
		for (int i = 0; i < 13; i++) {
			while (num >= values[i]) {
				ret += symbols[i];
				num -= values[i];
			}
		}
		return ret;	
	}
};
int main()
{
	Solution sol;

	return 0;

}
