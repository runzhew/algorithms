/*
{1,2,3,4,5,6} ---> {1,6,2,5,3,4}
{1,2,3,4,5,6,7} ---> {1,7,2,6,3,5,4}

One straightforward middle step of such reordering is:
{1,2,3,4,5,6}  --> {1,2,3,6,5,4} --> {1,6,2,5,3,4}
{1,2,3,4,5,6,7}---> {1,2,3,4,7,6,5} ---> {1,7,2,6,3,5,4}

*/

#include <iostream>
#include <vector>

using namespace std;

struct ListNode {
     int val;
     ListNode *next;
     ListNode(int x) : val(x), next(NULL) {}
};

void dump(ListNode *head) 
{
	while (head != NULL) {
		cout << head->val << " -> ";
		head = head->next;
	}
	cout << endl;
}

class Solution {
public:
	void reorderList(ListNode *head) {
		if (head == NULL || head->next == NULL) {
			return ;
		}

		// find the middle point, whether the size of list is odd or even
		ListNode *slow = head, *fast = head;
		while (fast->next != NULL && fast->next->next != NULL) {
			slow = slow->next;
			fast = fast->next->next;
		}


		// reverse the half of rear
		ListNode *cur = slow->next;
		ListNode *next = cur->next;
		// 头插法
		while (next != NULL) {
			cur->next = next->next;
			next->next = slow->next;
			slow->next = next;

			// update
			next = cur->next;
		}

		ListNode *p = slow->next;
		slow->next = NULL;

		// merge
		ListNode *q = head, *next_q = head->next;
		ListNode *next_p = p->next;
		while (true) {
			q->next = p;
			p->next = next_q;

			p = next_p;
			q = next_q;

			if (p == NULL)
				break;

			next_q = next_q->next;
			next_p = next_p->next;
		}
	}
};


int main()
{
	ListNode *head;
	ListNode dummy(-1);
	head = &dummy;
	ListNode *t = head;

	for (int i = 1; i < 3; i++) {
		t->next = new ListNode(i);
		t = t->next;
	}
	head = head->next;

	dump(head);

	Solution sol;
	sol.reorderList(head);

	dump(head);
	return 0;
}
