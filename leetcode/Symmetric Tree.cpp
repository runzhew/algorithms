/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	bool isSymmetric(TreeNode *root) {
		if (root == NULL) {
			return true;
		}

		return same_tree(root->left, root->right);
	}

	bool same_tree(TreeNode *t1, TreeNode *t2) {
		if (t1 == NULL && t2 != NULL)
			return false;
		if (t1 != NULL && t2 == NULL) 
			return false;
		if (t1 == NULL && t2 == NULL)
			return true;

		if (t1->val != t2->val)
			return false;

		return same_tree(t1->left, t2->right) && 
			same_tree(t1->right, t2->left);
	}
};
