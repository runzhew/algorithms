class Solution {
public:
	int atoi(string str)
	{
		if (str.size() == 0) {
			return 0;
		}
		// trim the space on head and tail
		int start = 0;
		int end = str.size()-1;
		bool neg = false;

		while (str[start] == ' ') {
			start++;
		}

		while (str[end] == ' ') {
			end--;
		}
		// + or -
		if (str[start] == '-') {
			neg = true;
			start++;	
		}
		else if (str[start] == '+') {
			start++;
		}

		//get the new string
		string new_str = str.substr(start, end-start+1);

		int result = 0;
		int i = 0;

		while (i < new_str.size()) {
			if (new_str[i] < '0' || new_str[i] > '9') {
				break;
			}
			int digit = new_str[i] - '0';

			if (neg == true && result > -((INT_MIN + digit)/10)) {
				return INT_MIN;
			}

			if (neg == false && result > ((INT_MAX - digit)/10)) {
				return INT_MAX;
			}
			
			result = result * 10 + digit;
			i++;
		}

		return neg ? -result : result;

	}

};


