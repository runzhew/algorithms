/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder)
	{

		TreeNode *root = NULL;
		int preSize = preorder.size();
		int inSize = inorder.size();

		root = __bulidTree(preorder, 0, preSize-1, inorder, 0, inSize-1);

		return root;
	}


	TreeNode * __bulidTree(vector<int> &preorder, int preStart, int preEnd,
			       vector<int> &inorder,  int inStart,  int inEnd) 
	{
		if (preStart > preEnd || inStart > inEnd) 
			return NULL;
			
		int rootVal = preorder[preStart];
		TreeNode *root = new TreeNode(rootVal);
		
		if (preStart == preEnd && inStart == inEnd)
		    return root;
		
		int rootPosition;

		for (int i = inStart; i <= inEnd; i++) {
			if (rootVal == inorder[i]) {
				rootPosition = i;
				break;
			}
		}

		//reset start and end 

		root->left = __bulidTree(preorder, preStart+1, preStart+(rootPosition-inStart),
					 inorder,  inStart, rootPosition-1);

		root->right = __bulidTree(preorder, preStart+(rootPosition-inStart)+1, preEnd,
					  inorder,  rootPosition+1, inEnd);
					  
		return root;
	}
};
