#include <iostream>
#include <stack>

using namespace std;

class Solution {
	public:
		bool isValid(string s) {
			stack<char> st;
			const char *p = s.c_str();

			for (int i = 0; i < s.length(); i++) {
				switch (p[i]) {
				case '(':
					st.push('('); 
					break;
				case '{':
					st.push('{'); 
					break;
				case '[':
					st.push('['); 
					break;
				case ')': 
					if (st.empty() || st.top() != '(')
						return false;
					else 
						st.pop();
					break;
				case ']':
					if (st.empty() || st.top() != '[') {
						return false;
					}
					else 
						st.pop();
					break;
				case '}':
					if (st.empty() || st.top() != '{')
						return false;
					else 
						st.pop();
					break;
				}	
			}

			if (st.empty())
				return true;
			else 
				return false;

		}
};


int main(void)
{
	Solution sol;
	sol.isValid("()[]{}");

	return 0;
}
