#include <iostream>

using namespace std;

class Solution {
public:
	int removeElement(int A[], int n, int elem) {

		int index = 0;
		int cur = 0;

		while (index < n && A[index] != elem) {
			index++;
		}

		cur = index;

		while (cur < n) {
			while (cur < n && A[cur] == elem) {
				cur++;
			}

			if (cur < n) {

				exchange(&A[index], &A[cur]);
				index++;
			}
		}

		return index;

	}

	void exchange(int * a, int *b) {
		int tmp = *a;
		*a = *b;
		*b = tmp;
	}
};


int main(void)
{
	Solution sol;
	int A[7] = {6, 4, 3, 5, 5 ,4, 5};
	cout << sol.removeElement(A, 7, 5) << endl;

	cout << "out " ;
	for (int i = 0; i < 7; i++) 
		cout << A[i];

	return 0;
}


