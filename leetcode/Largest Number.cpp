#include <iostream>
#include <sstream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
	string largestNumber(vector<int> &num) {
		string result;
		int n = num.size();

		while (n > 0) {
			for (int i = 0; i < n-1; i++) {
				if (compare(num[i], num[i+1]) == true) {
					//exchange(num[i], num[i+1]);
					int t = num[i];
					num[i] = num[i+1];
					num[i+1] = t;
				}
			}
			n--;
		}

        	n = num.size()-1;
        
		for (int i = n; i >=0; i--) {
			result += to_string(num[i]);
		}
		
	    	// just for "00" case 
		if (result.size() > 1 && result.substr(0, 2) == "00") 
			return "0";

		return result;

	}

	bool compare(int x, int y) 
	{
		stringstream ss;
		long long ab, ba;

		string xy = to_string(x) + to_string(y);
		string yx = to_string(y) + to_string(x);

		ss << xy;  ss >> ab;
		ss.clear();
		ss << yx;  ss >> ba;

		//cout << "ab = " << ab << ", ba = " << ba << endl;
		return ab > ba ? true : false;
	}
};


int main()
{
	Solution sol;
	sol.compare(30, 33);

	return 0;
}
