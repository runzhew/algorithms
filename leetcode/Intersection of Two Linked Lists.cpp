/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
	ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {

		if (headA == NULL || headB == NULL)
			return NULL;

		ListNode *p = headA;
		while (p->next != NULL) 
			p = p->next;

		p->next = headA;

		ListNode *fast = headB, *slow = headB;
		ListNode *slow2 = headB;

		while (fast != NULL && fast->next != NULL) {
			slow = slow->next;
			fast = fast->next->next;

			if (slow == fast) {
				while (slow != slow2) {
					slow = slow->next;
					slow2 = slow2->next;
				}
				p->next = NULL;
				return slow;
			}
		}
		p->next = NULL;
		return NULL;
	}
};
