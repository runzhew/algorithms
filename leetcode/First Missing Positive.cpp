#include <iostream>

using namespace std;

class Solution {
public:
    int firstMissingPositive(int A[], int n) { 
        int i = 0;
        while(i < n) {
            if (A[i] >= 0 && A[i] < n && A[A[i]-1] != A[i]) {
                int tmp = A[i];
                A[i] = A[tmp-1];
                A[tmp-1] = tmp;
            }
            else 
                i++;
        }
        
        for (int i = 0; i < n; i++) {
            if (A[i]-1 != i) 
                return i+1;
        }
        return n+1;
        
    }
};

int main()
{
	Solution sol;
	int A[] = {0, 1, 2};

	cout << sol.firstMissingPositive(A, 3);

	return 0;
}
