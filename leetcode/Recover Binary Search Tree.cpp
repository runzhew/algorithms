/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	void recoverTree(TreeNode *root) {

		vector<int> ele;
		vector<TreeNode *> point;

		inorder_travel(root, ele, point);

		sort(ele.begin(), ele.end());

		for (int i = 0; i < ele.size(); i++) {
			point[i]->val = ele[i];
		}
	}

	void inorder_travel(TreeNode * root, 
			    vector<int> &ele, vector<TreeNode *> &point) {

		stack<TreeNode *> st;
		TreeNode *p = root;

		while (!st.empty() || p != NULL) {
			while (p != NULL) {
				st.push(p);
				p = p->left;
			}

			TreeNode *t = st.top();
			st.pop();
			ele.push_back(t->val);
			point.push_back(t);

			if (t->right != NULL) {
				p = t->right;
			}
			else {
				p = NULL;
			}
		}
	}
};
