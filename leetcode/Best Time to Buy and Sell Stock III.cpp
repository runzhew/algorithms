/*股票交易的特性： 两次交易不会重叠，有点像 区间 的那道题目 
区间的头尾可以重叠， 比如 (1, 2, 3)

第一次交易(1, 2)
第二次交易(2, 3)

符合股票的交易规则
*/

class Solution {
public:
    int maxProfit(vector<int> &prices) {
        int n = prices.size();
        if (n < 2) {
            return 0;
        }
        vector<int> left(n, 0);
        vector<int> right(n, 0);
        
        int max, min, diff;
        
        // left half
        min = prices[0];
        for (int i = 1; i < n; i++) {
            diff = prices[i] - min;
            left[i] = max_num(diff, left[i-1]);
            if (diff < 0) {
                min = prices[i];
            }
        }
        
        // right half
        max = prices[n-1];
        for (int i = n-2; i >= 0; i--) {
            diff = max - prices[i];
            right[i] = max_num(diff, right[i+1]);
            if (diff < 0) {
                max = prices[i];
            }
        }
        
        // calculate 
        int ret = 0;
        for (int i = 0; i < n; i++) {
            int t = left[i] + right[i];  // not left[i] + right[i+1]
            if (t > ret) {
                ret = t;
            }
        }
        
        return ret;
    }
    
    int max_num(int a, int b)
    {
        if (a > b) {
            return a;
        }
        return b;
    }
    
};
