#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
	vector<int> getRow(int rowIndex) {
		vector<int> ret;
		vector<int> tmp;
		int index = 0;

		tmp.push_back(1);
		if (rowIndex == 0)
			return tmp;

		tmp.push_back(1);
		if (rowIndex == 1)
			return tmp;

		index = 2; // due to the two if case
		while (true) {
			ret.clear();
			ret.push_back(1);
			for (int i=1; i < tmp.size(); i++) {
				ret.push_back(tmp[i] + tmp[i-1]);
			}
			ret.push_back(1);

			if (index == rowIndex) {
				break;
			}
			else {
				tmp.swap(ret);
			}

			index++;
		}
		return ret;
	}
};

int main()
{
	Solution sol;
	vector<int> ret;
	vector<int>::iterator it;

	ret = sol.getRow(2);
	for (it = ret.begin(); it != ret.end(); it++) {
		cout << *it << ", ";
	}
	cout << endl;

	return 0;
}
