class Solution {
public:
	vector<TreeNode *> generateTrees(int n) 
	{
		vector<TreeNode *> result;

		result = _generateTree(1, n);

		return result;
	}

	
	vector<TreeNode *> _generateTree(int start, int end)
	{
		vector<TreeNode *> ret;

		if (start > end) {
		    ret.push_back(NULL);
		    return ret;
		}


		for (int i = start; i <= end; i++) {
			vector<TreeNode*> left =  _generateTree(start, i-1);
			vector<TreeNode*> right = _generateTree(i+1, end);

			for (int m = 0; m < left.size(); m++) {
				for (int n = 0; n < right.size(); n ++) {
					TreeNode * node = new TreeNode(i);
					node->left = left[m];
					node->right = right[n];
					ret.push_back(node);
				}
			}
		}

		return ret;
	}

};
