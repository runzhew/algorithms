#include <iostream>
#include <algorithm>

using namespace std;

class Solution {
	public:
		int reverse(int x) {
			bool pos = false;
			int num = abs(x);
			
			if (x < 0) {
				pos = true;
			}

			unsigned long res = 0;
			
			while (num != 0) {
				res = num % 10 + res * 10;
				if (res > INT_MAX)
				    return 0;
				num = num/10;
			}
			
			if (pos)
				return -res;
			return res;
		
		}
};



/*
貌似这个版本处理的更加好一些，用了 long long int 并判断 sum <INT_MAX

class Solution {
	public:
		int reverse(int x) {

			vector<char> numbers;
			long long int sum = 0 ;
			bool negative = false;

			if( x < 0 )
			{
				negative = true;
				x = -x;
			}

			while( x!=0 )
			{
				numbers.push_back(x%10+’0′);
				x /= 10;
			}

			for(int i = 0; i< numbers.size();i++)
			{
				sum = 10*sum +numbers[i]-’0′;
				assert(sum <INT_MAX);
			}
			if(negative)
				sum = -sum;

			return static_cast<int>(sum);
		}
};
*/

/*
题目中提出如何处理溢出问题，网上找的答案，貌似只是简单的return -1;

class Solution {
public:
	int reverse(int x) {
		bool positive = (x > 0) ? true : false;
		int result = 0;
		x = abs(x);
		while(x > 0){
			result = result * 10 + x % 10;
			x = x / 10;
		}
		if(result < 0){
			return -1;
		}
		if(!positive){
			result *= -1;
		}
		return result;
	}
}; */

int main(void)
{
	Solution sol;
	cout << sol.reverse(1000000003) << endl;

	return 0;
}
