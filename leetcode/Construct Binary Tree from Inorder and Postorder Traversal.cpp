class Solution {
public:
	TreeNode *buildTree(vector<int> &inorder, vector<int> &postorder)
	{
		if (inorder.size() == 0) 
			return NULL;

		TreeNode * root = NULL;

		root = __buildTree(inorder, 0, inorder.size()-1,
				   postorder, 0, postorder.size()-1);

		return root;

	}

	TreeNode *__buildTree(vector<int> &inorder, int inStart, int inEnd,
			      vector<int> &postorder, int postStart, int postEnd)
	{
		if (inStart > inEnd || postStart > postEnd) {
			return NULL;
		}

		int rootVal = postorder[postEnd];
		TreeNode *root = new TreeNode(rootVal);

		if (inStart == inEnd && postStart == postEnd)
			return root;

		// find rootVal in Inorder 
		int rootPosition;

		for (int i = inStart; i <= inEnd; i++) {
			if (inorder[i] == rootVal) {
				rootPosition = i;
				break;
			}
		}

		root->left = __buildTree(inorder, inStart, rootPosition-1,
					 postorder, postStart, postStart+(rootPosition-inStart)-1);

		root->right = __buildTree(inorder, rootPosition+1, inEnd,
					  postorder, postStart+(rootPosition-inStart), postEnd-1);


		return root;

	}
};
