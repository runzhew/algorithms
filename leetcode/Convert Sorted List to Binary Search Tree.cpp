/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	TreeNode *sortedListToBST(ListNode *head) {

		ListNode * t = head;
		TreeNode * root = NULL;
		int size = 0;

		if (head == NULL)
			return root;

		while (t != NULL) {
			size++;
			t = t->next;
		}
		
		root = traveler(head, 0, size-1);

		return root;
	}

	TreeNode * traveler(ListNode *head, int start, int end) 
	{
		if (start > end) {
			return NULL;
		}

		int mid = (start + end) / 2;

		ListNode *t = head;
		for (int i = start; i < mid; i++) {
			t = t->next;
		}

		TreeNode *root = new TreeNode(t->val);

		root->left = traveler(head, start, mid-1);
		root->right = traveler(t->next, mid+1, end);

		return root;
	}

};
