class Solution {
public:
	string countAndSay(int n) {
		if (n < 1)
			return "";

		string prev = "1";

		for (int i = 2; i <= n; i++) {
			char cur = prev[0];
			int times = 1;
			string tmp;
			prev.push_back('#');
			for (int k = 1; k < prev.size(); k++) {
				if (prev[k] == cur) {
					times++;
				}
				else {
					tmp += to_string(times);
					tmp.push_back(cur);
					cur = prev[k];
					times = 1;
				}
			}
			prev = tmp;
		}
		return prev;
	}
};
