#include <iostream>
#include <algorithm>
#include <string>

using namespace std;


class Solution {
public:
    string addBinary(string a, string b) {
        
        int m = a.size()-1;
        int n = b.size()-1;
        
        string res;
        int carry = 0;
        
        while (m >= 0 && n >= 0) {
            
            int t = (a[m]-'0') + (b[n]-'0') + carry;
            carry = 0;
            
            if (t >= 2) {
                t %= 2;
                carry = 1;
            }
            res += ('0'+t);
            
            m--;
            n--;
        }
        
        while ( m >= 0) {
            int t = (a[m]-'0') + carry;
            carry = 0;
            
            if (t >= 2) {
                t %= 2;
                carry = 1;
            }
            res += ('0'+t);
            m--;
        }
        
        while ( n >= 0) {
            int t = (b[n]-'0') + carry;
            carry = 0;
            
            if (t >= 2) {
                t %= 2;
                carry = 1;
            }
            res += ('0'+t);
            n--;
        }
        
        if (carry == 1) {
            res += '1';
        }
        
        reverse(res.begin(), res.end());
        
        return res;
        
    }
};


class Solution {
public:
	string addBinary(string a, string b) {
		string result;
		int ia = a.size()-1;
		int ib = b.size()-1;
		int carry = 0;
		
		if (a.size() == 0)
			return b;
		if (b.size() == 0) 
			return a;

		while (ia >= 0 && ib >= 0) {
			int tmp = (a[ia]-'0') + (b[ib]-'0') + carry;
			if (tmp >= 2) {
				carry = 1;
				tmp = tmp % 2;
				result = result + char('0'+tmp);
			}
			else {
				carry = 0;
				result = result + char('0'+tmp);
			}
			ia--;
			ib--;
		}

		while (ia >= 0) {
			int tmp = (a[ia]-'0')+carry;
			if (tmp >= 2) {
				carry = 1;
				tmp = tmp % 2;
				result += char('0'+tmp);
			}
			else {
				carry = 0;
				result += char('0'+tmp);
			}
			ia--;
		}
		while (ib >= 0) {
			int tmp = (b[ib]-'0')+carry;
			if (tmp >= 2) {
				carry = 1;
				tmp = tmp % 2;
				result += char('0'+tmp);
			}
			else {
				carry = 0;
				result += char('0'+tmp);
			}
			ib--;
		}

		if (carry == 1) {
			result += '1';
		}

		reverse(result.begin(), result.end());

		return result;
	}
};

int main()
{
	Solution sol;
	string a = "1011";
	string b = "1111";

	cout << "a=" << a << endl;
	cout << "b=" << b << endl;
	cout << sol.addBinary(a, b) << endl;

	return 0;
}
