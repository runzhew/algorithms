/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	bool hasPathSum(TreeNode *root, int sum) {
		int tmp_sum = 0;
		if (root == NULL)
			return false;

		return traveler(root, tmp_sum, sum);
	}

private:
//	bool traveler(TreeNode *root, int tmp_sum, int sum)
//	{
//		if (root == NULL) {
//			if (tmp_sum == sum)
//				return true;
//			else 
//				return false;
//		}
//
//		tmp_sum += root->val;
//		
//
//		if (traveler(root->left, tmp_sum, sum)) return true;
//		if (traveler(root->right, tmp_sum, sum)) return true;
//
//		return false;
//	}
	
	bool traveler(TreeNode *root, int tmp_sum, int sum)
	{
		if (root->left == NULL && root->right == NULL) {
			if (tmp_sum + root->val  == sum)
				return true;
			else 
				return false;
		}

		tmp_sum += root->val;
		

		if (root->left != NULL) {
			if (traveler(root->left, tmp_sum, sum)) 
				return true;
		}
		if (root->right != NULL) {
			if (traveler(root->right, tmp_sum, sum))
				return true;
		}

		return false;
	}
};
