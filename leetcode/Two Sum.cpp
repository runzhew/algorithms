#include <iostream>
#include <vector>
#include <map>

using namespace std;

vector<int> twoSum(vector<int> &number, int target) {
	map<int, int> mapping;
	vector<int>  result;

	for(int i = 0; i < number.size(); i++) {
		mapping[number[i]] = i;
	}

	for(int i = 0; i < number.size(); i++) {
		int searched = target - number[i];
		if (mapping.find(searched) != mapping.end()) {
			if (i != mapping[searched]) {
				result.push_back(i+1);
				result.push_back(mapping[searched] + 1);
			}
		}
	}
	return result;
}

int main(void) 
{
	vector<int> num;
	num.push_back(3);
	num.push_back(2);
	num.push_back(4);

	vector<int> ret = twoSum(num, 6);

	cout << "(" << ret[0] << ", " << ret[1] << ")" << endl;
	return 0;
}
