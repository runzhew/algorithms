#include <iostream>

using namespace std;

struct ListNode {
	int val;
	ListNode *next;
	ListNode(int v): val(v), next(NULL){}
};

class Solution {
public:
    ListNode *reverseBetween(ListNode *head, int m, int n) {
        
        ListNode dummy(-1);
        dummy.next = head;
        head = &dummy;
        
        ListNode * first = head, *second = head, *next, *pre;
        
        for(int i = 0; i < n; i++) {
            second = second->next;
        }
        for (int i = 0; i < m; i++) {
            pre = first;
            first = first->next;
        }
        
        pre->next = second;
        next = first->next;
        while (first != second) {
            first->next = second->next;
            second->next = first;
            first = next;
            next = next->next;
        }
        
        return dummy.next;
    }
};



void dump(ListNode *head) 
{
	while (head != NULL) {
		cout << head->val << " -> ";
		head = head->next;
	}
	cout << endl;
}

class Solution {
public:
	ListNode *reverseBetween(ListNode *head, int m, int n) {
		ListNode dummy(-1);
		dummy.next = head;
		head = &dummy;

		for (int i = 0; i < m-1; i++) {
			head = head->next;
		}

		ListNode *prev = head->next;
		ListNode *cur = prev->next;

		for (int i = m; i < n; i++) {
			// do the reverse
			//
			// the order is very important !!!!
			//
			prev->next = cur->next;
			cur->next = head->next;
			head->next = cur;

			// reset cur to the proper position
			cur = prev->next;
			dump(dummy.next);
		}
		return dummy.next;
	}
};

int main()
{
	ListNode *head = new ListNode(0);
	ListNode *next = head;
	ListNode *nd;
	for (int i = 1; i < 10; i++) {
		nd = new ListNode(i);
		next->next = nd;
		next = next->next;
	}
	next = head;
	head = head->next;
	delete next;

	dump(head);

	Solution sol;

	head = sol.reverseBetween(head, 4, 7);

	dump(head);

	return 0;
}

