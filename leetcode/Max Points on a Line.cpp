/**
 * Definition for a point.
 * struct Point {
 *     int x;
 *     int y;
 *     Point() : x(0), y(0) {}
 *     Point(int a, int b) : x(a), y(b) {}
 * };
 */
class Solution {
public:
	int maxPoints(vector<Point> &points) {

		map<double, int> slop;
		int max = 0, tmp_max = 0;
		int axisX , axisY;

		samePoints = 0;
		
		if (points.size() < 2) 
		    return points.size();

		for (int i = 0; i < points.size(); i++) {

			slop.clear();
			tmp_max = 0;

			for (int j = 0; j < points.size(); j++) {
				if (j == i)
					continue;

				axisX = points[j].x - points[i].x;
				axisY = points[j].y - points[i].y;

				slop_add(slop, axisX, axisY, tmp_max);
			}

			if (max < tmp_max) 
				max = tmp_max;
		}
		return max;
	}

private:
	int samePoints;

	void slop_add(map<double, int> &slop, int axisX, int axisY, int &tmp_max)
	{
		if (axisX == 0 && axisY != 0) {
			if (slop.find((double)INT_MAX) == slop.end()) {
				slop[(double)INT_MAX] = 2;
			}
			else {
				slop[(double)INT_MAX] ++;
			}
			if (slop[(double)INT_MAX] > tmp_max) {
				tmp_max = slop[(double)INT_MAX];
			}
		}
		// just to deal with Same Points
		//
		// take a lot of time to deal with the same point
		else if (axisX ==0 && axisY == 0) {
			if (tmp_max == 0) {
			    tmp_max = 2;
			}
			else {
			    tmp_max ++;
			}
		}
		else {
			double t = (0.1 * axisY) / axisX;
			if (slop.find(t) == slop.end()) {
				slop[t] = 2;
			}
			else {
				slop[t]++;
			}

			if (slop[t] > tmp_max) {
				tmp_max = slop[t];
			}
		}
	}
};
