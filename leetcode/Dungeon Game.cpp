class Solution {
public:
	int calculateMinimumHP(vector<vector<int> > &dungeon) {
		int m = dungeon.size();
		int n = dungeon[0].size();

		if (m == 0)
			return 0;

		vector<vector<int> > dp(m, vector<int>(n, 0));

		dp[m-1][n-1] = max(1, 1-dungeon[m-1][n-1]);

		for (int i = m-2; i >= 0; i--) {
			int t = dp[i+1][n-1]-dungeon[i][n-1];
			if (t > 0) {
				dp[i][n-1] = t;
			}
			else {
				dp[i][n-1] = 1;
			}
		}

		for (int i = n-2; i >= 0; i--) {
			int t = dp[m-1][i+1] - dungeon[m-1][i];
			if (t > 0) {
				dp[m-1][i] =  t;
			}
			else {
				dp[m-1][i] = 1;
			}
		}

		for (int i = m-2; i >= 0; i--) {
			for (int j = n-2; j >= 0; j--) {
				int t = min(dp[i+1][j], dp[i][j+1]) - dungeon[i][j];
				if (t > 0) {
					dp[i][j] = t;
				}
				else {
					dp[i][j] = 1;
				}
			}
		}

		return dp[0][0];
	}

	int max(int a, int b)
	{
		return a > b ? a : b;
	}

	int min(int a, int b)
	{
		return a < b ? a : b;
	}
};
