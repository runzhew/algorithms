#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
	vector<string> letterCombinations(string digits) {

		string tmp;
		
		if (digits.size() == 0) 
		    return result;

		for (int i = 0; i < digits.size(); i++) {
			input.push_back(digits[i]-'0');
		}

		dict.push_back(" "); 
		dict.push_back(""); 
		dict.push_back("abc"); 
		dict.push_back("def"); 
		dict.push_back("ghi"); 
		dict.push_back("jkl"); 
		dict.push_back("mno"); 
		dict.push_back("pqrs"); 
		dict.push_back("tuv"); 
		dict.push_back("wxyz"); 
		
		dfs(0, tmp);
		
		return result;
	}
private:
    vector<string> result;
    vector<int>  input;
    vector<string> dict;
	
	void dfs(int start, string tmp)
	{
	    if (start >= input.size()) {
	        result.push_back(tmp);
	        return ;
	    }
	        
	    for (int i = 0; i < dict[input[start]].size(); i++) {
	        dfs(start+1, tmp+dict[input[start]][i]);
	    }
	}
};
	

int main()
{
	Solution sol;
	vector<string> ret = sol.letterCombinations("02");
	for (int i = 0; i < ret.size(); i++) {
		cout << ret[i] << endl;
	}

	return 0;
}
