/*
遇到 * 的时候，用 star 记住 星号位置，并且，p 移动到下一个，
               用 ss 记住 当前s 的位置，s 不动 （ss 和 star 留着以后走不下去的时候用）
               
遇到 不匹配 且 star 存在， s 回到 ss 所指向的下一个， ss 也同步更新
                        p 回到 star 指向的下一个

*/


class Solution {
public:

bool isMatch(string &sss, string &ppp) {
        const char * s = sss.c_str();
        const char * p = ppp.c_str();
        const char* star = NULL;
        const char* ss = s;
        while (*s){
            //advancing both pointers when (both characters match) or ('?' found in pattern)
            //note that *p will not advance beyond its length 
            if ((*p == '?') || (*p == *s)) {
                s++;
                p++;
                continue;
            }

            // * found in pattern, track index of *, only advancing pattern pointer 
            if (*p == '*') {
                star = p;
                p++;
                ss = s;
                continue;
            }

            //current characters didn't match, last pattern pointer was *, current pattern pointer is not *
            //only advancing pattern pointer
            // 
            if (star) {
                ss++;
                s = ss;
                p = star+1;
                continue;
            }

           //current pattern pointer is not star, last patter pointer was not *
           //characters do not match
            return false;
        }

       //check for remaining characters in pattern
        while (*p == '*') {
            p++;
        }

        return !*p;  
    }
};

