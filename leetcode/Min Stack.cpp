#include <iostream>
#include <stack>

using namespace std;

class MinStack {
	public:
		void push(int x) {

			data.push(x);

			if (min.empty() == true) {
				min.push(x);
			}
			else if (min.top() >= x) {
				min.push(x);
			}
		}

		void pop() {


			if (!data.empty()) {
				if (data.top() == min.top()) {
					data.pop();
					min.pop();
				}
				else {
					data.pop();
				}
			}
		}

		int top() {

			return data.top();
		}

		int getMin() {
			return min.top();
		}

	private:
		stack<int> data;
		stack<int> min;
};

int main() 
{
	MinStack s;
	s.push(512);
	s.push(-1024);
	s.push(-1024);
	s.push(512);

	s.pop();
	cout << s.getMin() << endl;
	s.pop();
	cout << s.getMin() << endl;
	s.pop();
	cout << s.getMin() << endl;

	return 0;
}
