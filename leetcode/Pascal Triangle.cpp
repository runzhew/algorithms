
class Solution {
public:
	vector<vector<int> > generate(int numRows) {

		vector<vector<int>> result;
		vector<int> tmp;
		int index;

		if (numRows == 0) {
			return result;
		}

		tmp.push_back(1);
		result.push_back(tmp);

		if (numRows == 1) {
			return result;
		}

		tmp.clear();

		tmp.push_back(1);
		tmp.push_back(1);
		result.push_back(tmp);

		if (numRows == 2) {
			return result;
		}

		for (int i = 3; i <= numRows; i++) {
			tmp.clear();
			tmp.push_back(1);

			for (index = 1; index < i-1; index++) {
				temp = result[i-2][index-1] + result[i-2][index];
				tmp.push_back(temp);
			}
			tmp.push_back(1);
			result.push_back(tmp);
		}

		return result;
	}
};
