#include <iostream>
#include <stack>

using namespace std;

class Solution {
	public:
		void longestValidParentheses(string s) {
			const char *p = s.c_str();

			stack<int> st;
			for (int i = 0; i < s.length(); i++) {
				if (p[i] == '(') {
					st.push(i);
				} 
				//the problem guarantee else is ')'
				//st.top is ) or stack is empty
				else if (st.empty()) {
					st.push(i);
				}
				else if (p[st.top()] == ')') {
					st.push(i);
				}
				else{ // st.top is (, and we have a ), so pop
					st.pop();
				}
			}
			cout << dump(s, st) << endl;

		}
	private:
		int  dump(string &s, stack<int> st) {
			int max = 0, tmp = s.length();
			while (!st.empty()) {
				tmp = tmp - st.top() - 1;
				if (tmp >= max)
					max = tmp;
				tmp = st.top();
				st.pop();
			}
			if (tmp > max) 
				max = tmp;
			return max;
		}

};

int main(void)
{
	Solution sol;
	sol.longestValidParentheses("()))()))");
	return 0;
}
