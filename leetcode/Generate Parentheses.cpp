#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
	vector<string> generateParenthesis(int n) {
		string tmp;

		dfs(n, n, tmp);

		return result;
	}

private:
	vector<string> result;

	void dfs(int left, int right, string tmp)
	{
		if (left == 0 && right == 0) {
			result.push_back(tmp);
			return ;
		}
		else if (left > right) {
			return ;
		}
		else if (left < 0 || right < 0) {
			return ;
		}
		else {
			dfs(left-1, right, tmp+"(");
			dfs(left, right-1, tmp+")");
		}
	}
};


int main()
{
	Solution sol;
	vector<string> ret = sol.generateParenthesis(3);

	for (int i = 0; i < ret.size(); i++) {
		cout << ret[i] << endl;
	}

	return 0;
}
