class Solution {
public:
	void rotate(vector<vector<int> > &matrix) {

		// remember how to rotate the matrix !!!!

		// How to : rotate by the main diagonal ?
		// How to : rotate by the vice diagonal ?
		// How to : switch by the vertical middle line ?
		// How to : switch by the horizontal ?
		// 
		//
		// here I chosse rotate by vice diagonal and then switch by horizontal

		int i, j;
		int n = matrix.size();

		for (i = 0; i < n; i++) {
			for (j = 0; j < n-i; j++) {
				swap(&matrix[i][j], &matrix[n-1-j][n-1-i]);
			}
		}

		// by horizontal  
		for (i = 0; i < n/2; i++) {
			for (j = 0; j < n; j++) {
				swap(&matrix[i][j], &matrix[n-1-i][j]);
			}
		}


	}

	void swap(int *a , int *b)
	{
		int t = *a;
		*a = *b;
		*b = t;
	}
};
