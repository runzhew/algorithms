// Search in Rotated Sorted Array II 和
// Search in Rotated Sorted Array  的代码都可以用这个版本来解决，这是一个通用的代码
// 这个版本的代码与PDF II 题的解法一致，但是也适用于 Search in Rotated Sorted Array

// 这个代码的好处是 分开判断 (A[first] < > =  A[mid]) 的三种关系，更加清楚明朗

class Solution {
public:
    bool search(int A[], int n, int target) {
        		int first = 0, last = n-1;
		int mid;

		while (first <= last) {

			mid = (first + last) / 2;

			if (target == A[mid]) 
				return true;

			if (A[first] < A[mid]) {
				// this branch is sorted
				if (A[first] <= target &&  target <= A[mid]) {
					last = mid-1;
				}
				else {
					first = mid + 1;
				}
			}
			else if (A[first] > A[mid]) {
				// this branch is unsorted
				// so, we test the other branch, because the other 
				// branch must be sorted.
				if (A[mid] <= target && target <= A[last]) {
					first = mid+1;
				}
				else {
					last = mid-1;
				}
			}
			else {
			    first++;
			}
		}
		return false;
    }
};
