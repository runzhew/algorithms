class Solution {
public:
	int majorityElement(vector<int> &num) {
		int n = num.size();
		
		map<int, int> counter;
		map<int, int>::iterator it;

		for (int i = 0; i < n; i++) {
			counter[num[i]]++;
		}

		for (it = counter.begin(); it != counter.end(); it++) {
			if (it->second > n/2) {
				return it->first;
			}
		}
	}
};

