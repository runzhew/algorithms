#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

class Solution {
public:
	vector<string> anagrams(vector<string> &strs) {
		vector<string> result;
		string key;

		if (strs.size() == 0) 
			return result;

		map<string, vector<string> > groups;

		for (int i = 0; i < strs.size(); i++) {
			key = strs[i];
			sort(key.begin(), key.end());
			groups[key].push_back(strs[i]);
		}
		
		map<string, vector<string> > :: iterator  it;

		for (it = groups.begin(); it != groups.end(); it++) {
			if ((it->second).size() > 1) {
				result.insert(result.end(), it->second.begin(),
					      		it->second.end());
			}
		}
		return result;
	}
};
