#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Solution {
public:
    vector<vector<int> > combinationSum(vector<int> &candidates, int target) {
        
        vector<vector<int> > result;
        vector<int> tmp;
        
        sort(candidates.begin(), candidates.end());
        dfs(0, candidates, target, result, tmp);
        
        return result;
    }
    
    void dfs(int start, vector<int> &candidates, int target, 
            vector<vector<int> > &result, vector<int> &tmp) {
                
        if (target < 0) {
            return;
        }
        
        if (target == 0) {
            result.push_back(tmp);
            return ;
        }
        
        for (int i = start; i < candidates.size(); i++) {
            tmp.push_back(candidates[i]);
            dfs(i, candidates, target-candidates[i], result, tmp);
            tmp.pop_back();
        }
    }
};

class Solution1 {
public:
	vector<vector<int> > combinationSum(vector<int> &candidates, int target) {

		//sort(candidates.begin(), candidates.end(), greater<int>());

		int sum = 0;
		vector<int> tmp;

		dfs(0, candidates, target, tmp, sum);
		
		return result;
	}
private:
	vector<vector<int> > result;

	void dfs(int start, vector<int> &candidates, int target, vector<int> &tmp, int sum) {

		if (sum == target) {
			result.push_back(tmp);
			return ;
		}

		if (sum > target) {
			return ;
		}

		for (int i = start; i < candidates.size(); i++) {
			sum += candidates[i];
			tmp.push_back(candidates[i]);
			dfs(i, candidates, target, tmp, sum);
			sum -= candidates[i];
			tmp.pop_back();
		}
	}

};

int main()
{
	Solution sol;
	vector<int> cand;
	cand.push_back(8);
	cand.push_back(7);
	cand.push_back(4);
	cand.push_back(3);

	vector<vector<int> > ret = sol.combinationSum(cand, 11);

	for (int i = 0; i < ret.size(); i++) {
		for (int j = 0; j < ret[i].size(); j++) {
			cout << ret[i][j] << ", ";
		}
		cout << endl;
	}

	return 0;
}	
