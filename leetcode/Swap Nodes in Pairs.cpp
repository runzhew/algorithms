/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
	ListNode *swapPairs(ListNode *head) {

		if (head == NULL)
			return head;

		ListNode dummy(-1);
		dummy.next = head;
		head = &dummy;
		
		ListNode * prev = head, *cur = head->next, *next = cur->next;
		
		while (next != NULL) {
			prev->next = next;
			cur->next = next->next;
			next->next = cur; 	

			// 
			prev = cur;
			cur = cur->next;
			if (cur == NULL) {
				next = NULL;
			}
			else {
				next = cur->next;
			}
		}
		head = head->next;
		return head;
	}
};
