#include <iostream>
#include <vector>

using namespace std;
class Solution {
public:
	int candy(vector<int> &ratings) {
	
		int n = ratings.size();
		vector<int> num(ratings.size(), 1);

		// from left to right scan
		for (int i = 1; i < n; i++) {
			if (ratings[i] > ratings[i-1]) {
				if (num[i] <= num[i-1]) {
					num[i] = num[i-1]+1;
				}
			}
		}

		// from right to left scan
		for (int i = n-2; i >= 0; i--) {
			if (ratings[i] > ratings[i+1]) {
				if (num[i] <= num[i+1]) {
					num[i] = num[i+1]+1;
				}
			}
		}

		int sum = 0;
		for (int i = 0; i < n; i++) {
			sum += num[i];
		}
		return sum;
	}
private:
	void dump(vector<int> &num) {
		for (int i = 0; i < num.size(); i++) {
			cout << num[i] << ", ";
		}
		cout << endl;
	}
};

/*
class Solution {
public:
	int candy(vector<int> &ratings) {

		vector<int> num(ratings.size(), 1);

		int n = ratings.size();

		int min = ratings[0];
		int idx = 0;
		for (int i = 0; i < ratings.size(); i++) {
			if (ratings[i] < min) {
				idx = i;
				min = ratings[i];
			}
		}
		
		cout << "idx = " << idx << endl;

		int i, j;
		for (i = idx+1; i <ratings.size()-1; i++) {
			if (ratings[i] > ratings[i-1]) {
				if (num[i] <= num[i-1]) {
					num[i] = num[i-1]+1;
				}
			}

			if (ratings[i] > ratings[i+1]) {
				if (num[i] <= num[i+1]) {
					num[i] = num[i+1]+1;
				}
			}
		}
		//last element
		if (ratings[n-1] > ratings[n-2]) {
			if (num[n-1] <= num[n-2]) {
				num[n-1] = num[n-2] + 1;
			}
		}

		for (j = idx-1; j > 0; j--) {
			if (ratings[j] > ratings[j+1]) {
				if (num[j] <= num[j+1]) {
					num[j] = num[j+1]+1;
				}
			}

			if (ratings[j] > ratings[j-1]) {
				if (num[j] <= num[j-1]) {
					num[j] = num[j-1]+1;
				}
			}
		}

		// first element 
		if (ratings[0] > ratings[1]) {
			if (num[0] <= num[1]) {
				num[0] = num[1] + 1;
			}
		}


		int sum = 0;
		for (int i = 0; i < n; i++) {
			cout << num[i] << ", ";
			sum += num[i];
		}
		cout <<endl;

		return sum;
	}
};
*/

int main()
{
	Solution sol;
	vector<int> rate;
	rate.push_back(1);
	rate.push_back(3);
	rate.push_back(4);
	rate.push_back(3);
	rate.push_back(2);
	rate.push_back(1);

	cout << sol.candy(rate) << endl;

	return 0;
}

