class Solution {
public:
	void sortColors(int A[], int n) {

		int idx0 = 0;
		int idx2 = n-1;

		for (int i = 0; i < n; i++) {
			if (A[i] == 0) {
				swap(A[i], A[idx0]);
				idx0++;
				continue; //continue 很重要！！！
			}

			if (A[i] == 2) {
				swap(A[i], A[idx2]);
				idx2--;
				continue;
			}
		}
	}
};



class Solution {
public:
    void sortColors(int A[], int n) {
        
        int t[3] = {0, 0, 0};
        
        for (int i = 0; i < n; i++) {
            t[A[i]]++;
        }
        
        int k = 0;
        for (int i = 0; i < 3; i++) {
            for(int j = k; j < t[i]+k; j++) {
                A[j] = i;
            }
            k = t[i] + k;
        }
    }
};
