class Solution {
public:
	int ladderLength(string start, string end, unordered_set<string> &dict) {

		if (start.size() == 0 || end.size() == 0) {
			return 0;
		}

		if (start.size() != end.size()) {
			return 0;
		}

		queue<string> next, current;
		set<string> visited;
		bool found = false;
		int level = 0;
		
		current.push(start);
		while (!current.empty() && !found) {
			level++;
			while (!current.empty() && !found) {
				string str(current.front());
				current.pop();
				
				for (int i = 0; i < str.size(); i++) {

					string new_word = str;

					for (char c = 'a'; c <= 'z'; c++) {
						if (c == new_word[i]) {
							continue;
						}
						swap(c, new_word[i]);

						if (new_word == end) {
							found = true;
							return level+1;
						}

						if (dict.find(new_word) != dict.end() &&
						   visited.find(new_word) == visited.end()) {
							next.push(new_word);
							visited.insert(new_word);
						}
						swap(c, new_word[i]);
					}
				}
			}
			swap(next, current);
		}

		return 0;
	}
};
