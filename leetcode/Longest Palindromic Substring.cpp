#include <iostream>
#include <vector>
#include <cstring>

using namespace std;

class Solution {
public:
	string longestPalindrome(string s) {
		int n = s.size();
		//vector<vector<int> > note(n, vector<int>(n, 0));
		int note[n][n];

		int len = 2;

		memset(note, 0, sizeof(note));

		for (int i = 0; i < n; i++) {
			note[i][i] = 1;
		}

		// DP from length = 2
		//
		
		int start = 0, max = 1; // max = 1, for the case of 'a'

		for (len = 2; len <= n; len++) {
			for (int i = 0; i < n-len+1; i++) {
				int j = i+len-1;
				if (s[i] == s[j]) {
					if (i+1 == j) {// if they are neighbor
						note[i][j] = 2;
					}
					else if (note[i+1][j-1] == 0){
						note[i][j] = 0;
					}
					else {
						note[i][j] = note[i+1][j-1] + 2;
					}

					if(note[i][j] > max) {
						max = note[i][j];
						start = i;
					}
				}
			}
		}

		// Time limited !!!!!!!
		// find the biggest num in note, that's the vaule we want

		//for (int i = 0; i < n; i++) {
		//	for (int j = 0; j < n; j++) {
		//		if (note[i][j] > max) {
		//			start = i;
		//			end = j;
		//			max = note[i][j];
		//		}
		//	}
		//}

		//cout << "start = " << start << " end = " << end << " max = " << max << endl;

		return s.substr(start, max);
	}

};


int main()
{
	Solution sol;
	string s = "civilwarranynartiWravelmenlivi";
	cout << "max palidrome = " << sol.longestPalindrome(s) << endl;

	return 0;
}
