#include <iostream>
#include <unordered_set>
#include <string>

using namespace std;

class Solution {
public:
	bool result;

	bool wordBreak(string s, unordered_set<string> &dict) {

		result = false;
		traveler(s, 0, dict);
		return result;
	}

	void traveler(string &s, int start, unordered_set<string> &dict) 
	{
		if (start == s.size()) {
			result = true;
		}

		for (int i = start; i < s.size(); i++) {
			for(int j = i; j < s.size(); j++) {
				if (dict.find(s.substr(start, j-i+1)) != dict.end()) {
					cout << "i = " << i << " , j = " << j << " s = " << s.substr(start, j-i+1) << endl;
					start = j+1;
					traveler(s, start, dict);
				}
			}
		}
	}

};

int main()
{
	Solution sol;
	string s = "aaaaaaa";
	unordered_set<string> dict;
	dict.insert("aaaa");
	dict.insert("aaa");

	if (sol.wordBreak(s, dict) == true) {
		cout << "True" << endl;
	}
	else {
		cout << "False" << endl;
	}

	return 0;
}
