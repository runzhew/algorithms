/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
    vector<vector<int>> levelOrder(TreeNode *root)
    {
        queue<TreeNode *> parent;
        queue<TreeNode *> child;

        TreeNode * p;

        vector<vector<int>> result;
        vector<int>  tmp;

        if (root == NULL)
            return result;

        parent.push(root);

        while (!parent.empty())
        {
            while (!parent.empty())
            {
                p = parent.front();
                parent.pop();

                tmp.push_back(p->val);
                
                if (p->left != NULL) 
                {
                    child.push(p->left);
                }
                
                if (p->right != NULL)
                {
                    child.push(p->right);
                }
                
            }

            result.push_back(tmp);
            tmp.clear();
            parent.swap(child);
        }

        return result;

    }
};
