#include <iostream>
#include <unordered_set>
#include <string>
#include <cstring>

using namespace std;

class Solution {
public:
	bool wordBreak(string s, unordered_set<string> &dict) {

		string t = "#" + s; // dummy
		int n = t.size();

		bool record[n]; 
		memset(record, false, sizeof(record));
		record[0] = true;

		for (int i = 1; i < n; i++) {
			for (int j = 0; j < i; j++) {
				record[i] = record[j] && (dict.find(s.substr(j+1, i-j)) != dict.end());
				if (record[i] == true) 
					break;
			}
		}

		return record[n-1];
	}
};

int main()
{
	Solution sol;
	string s = "a";
	unordered_set<string> dict;
	dict.insert("a");
	dict.insert("aaa");

	if (sol.wordBreak(s, dict) == true) {
		cout << "True" << endl;
	}
	else {
		cout << "False" << endl;
	}

	return 0;
}
