#include <iostream>
#include <vector>
using namespace std;

// an anther solution, I think it is easier to understand.
//

class Solution {
public:
	vector<int> spiralOrder(vector<vector<int> > &matrix) {
		vector<int> result;

		if (matrix.size() == 0) {
			return result; 
		}

		int beginx = 0, endx = matrix[0].size() - 1;
		int beginy = 0, endy = matrix.size() - 1;	

		int i, j;
		while (true) {
			// from left to right
			for (i = beginx; i <= endx; i++) {
				result.push_back(matrix[beginy][i]);
			}
			if (++beginy > endy) 
				break;
			// from top to buttom;
			for (i = beginy; i <= endy; i++) {
				result.push_back(matrix[i][endx]);
			}
			if (--endx < beginx) 
				break;
			// from right to left 
			for (i = endx; i >= beginx; i--) {
				result.push_back(matrix[endy][i]);
			}
			if (--endy < beginy) 
				break;

			// from buttom to top 
			for (i = endy; i >= beginy; i--) {
				result.push_back(matrix[i][beginx]);
			}
			if (++beginx > endx) 
				break;
		}

		return result;
	}
};



// solution 1
//
class Solution {
public:
	vector<int> spiralOrder(vector<vector<int> > &matrix) {

		if (matrix.size() == 0) {
			return vector<int>();
		}
		int m = matrix[0].size();
		int n = matrix.size();

		// pay attention to the constructor !!!!!
		vector<int> ret(m*n, 0);

		int layer = min(m, n);
		int index, i;

		for (index = 0; layer > 1; layer -= 2) {
			int offset = (min(m, n) - layer)/2;
			// left to right
			for (i = offset; i < m-offset-1; i++) {
				ret[index++] = matrix[offset][i];
			}	       
			// top to buttom
			for (i = offset; i < n-offset-1; i++) {
				ret[index++] = matrix[i][m-offset-1];
			}
			// right to left 
			for (i = m-offset-1; i > offset; i--) {
				ret[index++] = matrix[n-offset-1][i];
			}
			// buttom to top;
			for (i = n-offset-1; i > offset; i--) {
				ret[index++] = matrix[i][offset];
			}
		}

		if (layer == 1) {
			if (m < n) {
				for (i = m/2; i < n - m/2; i++) {
					ret[index++] = matrix[i][m/2];
				}
			}
			else {
				for (i = n/2; i < m - n/2; i++) {
					ret[index++] = matrix[n/2][i];
				}
			}
		}

		return ret;
	}
private:
	int min(int a, int b) {
		return (a < b) ? a : b;
	}
};


int main(void)
{
	Solution sol;
	vector<vector<int> > input;
	vector<int> output;
	output.push_back(1);
	output.push_back(2);
	input.push_back(output);
	output.clear();
	output.push_back(3);
	output.push_back(4);
	input.push_back(output);
	output.clear();

	output = sol.spiralOrder(input);

	for (int i = 0; i < output.size(); i++) {
		cout << output[i] << ", ";
	}

	return 0;
}
