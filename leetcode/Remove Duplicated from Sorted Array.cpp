
// solution one
//
// and the element to the end of the A[], 
// but, I think it is not the CONSTANT memory ?

class Solution {
public:
	int removeDuplicates(int A[], int n) {
		int dup = 0;
		int cur = 0;
		
		if (n < 2)
			return n;

		while (cur < n) {
			while (cur < n && A[cur] == A[dup])
				cur++;
			if (cur == n) {
				return dup+1;
			}
			A[dup+1] = A[cur];
			dup++;
		}
		return dup+1;
	}
};
