
// 一个数，肯定能被表示成几个质数的乘积
// http://www.mathsisfun.com/prime-factorization.html
// 这里， n = 2, 3, 5 ... 的乘积
// 又因为 10 = 2 * 5， 而且2肯定比5多，因此5的次方数决定了10的个数，从而决定0的个数


class Solution {
public:
    int trailingZeroes(int n) {
        
        int count = 0;
        while( n > 0) {    // 5  的倍数贡献 k 个 5，
                          //  25 的倍数贡献 k' 
                          // 125 的倍数贡献 k'' 个 5，
            count += n/5;
            n = n / 5;
        }
        return count;
    }
};



#include <iostream>

/*
 *
对N!进行质因数分解： 
N!=2X*3Y*5Z …，因为10=2*5，所以M与2和5的个数即X、Z有关。

每一对2和5都可以得到10，故M=min(X,Z)。
因为能被2整除的数出现的频率要比能被5整除的数出现的频率高，所以M=Z。
其实也很好推出，1-9 中两两相乘，末位有0的话必须要有5，其它的数则是2的倍数。

*/

//  http://www.acmerblog.com/factorial-zero-number-5683.html

class Solution {
public:
	int trailingZeroes(int n) {

		int count = 0;

		for (int i = 1; i <= n; i++) {
			if (i % 5 == 0) {
				int t = i / 5;
				count += t;
			}
		}
		return count;
	}
};

/*
 * [N/5] 表示不大于N的的数中5的倍数贡献一个5, 
 * [N/5^2] 表示不大于N的数中5^2的倍数在贡献一个5…
 *
 *  Z =[N/5] + [N/5^2] + [N/5^3] + …
 */

class Solution {
public:
int trailingZeroes(int n) {

	int count = 0;
	int base = 5;
	
	while(n/base >= 1) {
		count++;
		base *= 5;
	}

	return count;
}
};




