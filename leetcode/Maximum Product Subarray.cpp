class Solution {
public:
	int maxProduct(int A[], int n) {

		int local_max = A[0]; int local_min = A[0];
		int global_max = local_max; 

		for (int i = 1; i < n; i++) {
			int local_max_copy = local_max;
			local_max = max(local_max*A[i], A[i], local_min*A[i]);
			local_min = min(local_max_copy*A[i], A[i], local_min*A[i]);

			if (global_max < local_max)
				global_max = local_max;
		}

		return global_max;
	}

	int max(int a, int b, int c) 
	{
		int tmp;
		if (a > b) 
			tmp = a;
		else 
			tmp = b;

		if (tmp > c) 
			return tmp;
		else 
			return c;
	}

	int min(int a, int b, int c) 
	{
		int t;
		if (a < b)
			t = a;
		else 
			t = b;

		if (t < c)
			return t;
		else 
			return c;
	}
};
