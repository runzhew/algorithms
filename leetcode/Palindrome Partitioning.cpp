#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
	vector<vector<string> > partition(string s) {

		vector<vector<string> > result;
		vector<string> tmp;

		if (s.size() == 0)
			return result;

		int cut = s.size() - 1;

		// 0   represents the whole string is palindrom
		// cut represents every one of the single character is palindrom
		//
		dfs(0, tmp, result, s);

		return result;
	}

	void dfs(int start, vector<string> &tmp,
		 vector<vector<string> > &result, string &s)
	{ 
		if (start == s.size()) {
			result.push_back(tmp);
			return ;
		}

		for (int i = start; i < s.size(); i++) {
			if (isPalindrome(s, start, i)) {
				tmp.push_back(s.substr(start, i-start+1));
				dfs(i+1, tmp, result, s);
				tmp.pop_back();
			}
		}

	}

	bool isPalindrome(string &s, int start, int end)
	{
		while(start < end) {
			if (s[start++] != s[end--]) {
				return false;
			}
		}
		return true;
	}
};


int main()
{
	Solution sol;
	string s("abba");
	vector<vector<string> > ret = sol.partition(s);

	for (int i = 0; i < ret.size(); i++) {
		for (int j = 0; j < ret[i].size(); j++) {
			cout << ret[i][j] << ", ";
		}
		cout << endl;
	}
	cout << "end of program" << endl;
	return 0;
}
