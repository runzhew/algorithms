class Solution {
public:
	int minPathSum(vector<vector<int> > &grid) {

		int m = grid[0].size();
		int n = grid.size();
		int i, j;
		
		for (i = 1; i < n; i++) {
			grid[i][0] += grid[i-1][0];
		}

		for (j = 1; j < m; j++) {
			grid[0][j] += grid[0][j-1];
		}

		for (i = 1; i < n; i++) {
			for (j = 1; j < m; j++) {
				if (grid[i][j-1] < grid[i-1][j]) {
					grid[i][j] += grid[i][j-1];
				}
				else {
					grid[i][j] += grid[i-1][j];
				}
			}
		}

		return  sum;
	}

};

// 下面的这种解法是一个非常严重的错误，。是错误理解动态规划的典型例子。
//
// 重点关注动态规划 、 贪心算法
//

//class Solution {
//public:
//	int minPathSum(vector<vector<int> > &grid) {
//		int i = 0, j = 0;
//		int m = grid[0].size();
//		int n = grid.size();
//		int sum = grid[0][0];
//
//		while (i != m || j != n) {
//
//			if (i + 1 > m) { // only can go right
//				sum += grid[i][j+1];
//				j++;
//				continue;
//			}	
//			if (j + 1 > n) { //only can go down
//				sum += grid[i+1][j];
//				i++;
//				continue;
//			}
//
//			if (grid[i][j+1] < grid[i+1][j]) {
//				sum += grid[i][j+1];
//				j++;
//				continue;
//			}
//			else {
//				sum += grid[i+1][j];
//				i++;
//				continue;
//			}
//		}
//
//		return sum;
//	}
//};
