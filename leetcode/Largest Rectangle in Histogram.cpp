class Solution {
public:
	int largestRectangleArea(vector<int> &height) {

		int max = 0;

		for (int i = 0; i < height.size(); i++) {

			if (i+1 < height.size() && height[i] <= height[i+1]) {   // must <= !!!!!!
				continue;
			}

			int minHeight = height[i];
			for (int j = i; j >= 0; j--) {             // j = i; just for case : [1]
				if (height[j] < minHeight) {
					minHeight = height[j];
				}
				int tmp_max = minHeight * (i-j+1);

				if (tmp_max > max)
					max = tmp_max;
			}
		}
		return max;
	}
};
