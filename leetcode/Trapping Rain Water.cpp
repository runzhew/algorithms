/*
对于任何一个坐标，检查其左右的最大坐标，然后相减就是容积。所以，
1. 从左往右扫描一遍，对于每一个坐标，求取左边最大值。
2. 从右往左扫描一遍，对于每一个坐标，求最大右值。
3. 再扫描一遍，求取容积并加和。
#2和#3可以合并成一个循环
*/

class Solution {
public:
	int trap(int A[], int n) {

		if (n < 2) return 0;

		vector<int> left(n, 0);
		vector<int> right(n, 0);

		int leftMax = A[0];
		int rightMax = A[n-1];

		// special case 0
		left[0] = 0;
		for (int i = 1; i < n-1; i++) {
			left[i] = leftMax;
			if (A[i] >= leftMax) {
				leftMax = A[i];
			}
		}

		// special case n-1
		right[n-1] = 0;

		int sum = 0;

		for (int j = n-2; j > 0; j--) {
			right[j] = rightMax;
			if (A[j] > rightMax) {
				rightMax = A[j];
			}

			int ctrap = min(left[j], right[j]) - A[j];
			if (ctrap > 0) 
				sum += ctrap;

		}

		return sum;

	}
};
