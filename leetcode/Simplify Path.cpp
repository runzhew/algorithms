#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
	string simplifyPath(string path) {

		vector<string> result;
		int i = 0;

		while ( i < path.size()) {
			// filter the /
			while (path[i] == '/' && i < path.size()) 
				i++;
			if (i = path.size())
				break;

			// 
			int start = i;
			while (path[i] != '/' && i < path.size()) 
				i++;

			int end = i;
			string element = path.substr(start, end-start);

			cout << "start = " << start << "  end = " << end << " element = " << element << endl;

			if (element == "..") {
				if (result.size() > 0) {
					result.pop_back();
				}
			}
			else if (element != ".") {
				result.push_back(element);
			}
			i++;
		}

		//generate the path
		string ret;

		if (result.size() == 0)
			return "/";
		else {

			for (int k = 0; k < result.size(); k++) {
				ret += "/";
				ret += result[k];
			}
		}

		return ret;
	}
};


int main() 
{
	string t = "/.";
	Solution sol;
	cout << sol.simplifyPath(t) << endl;

	return 0;
}
