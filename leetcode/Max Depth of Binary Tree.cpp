class Solution {
public:
    int maxDepth(TreeNode *root) {
        max = 0;
        traveler(root, 0);
	return max;
    }
private:
    void traveler(TreeNode *root, int high)
    {
	    if (root == NULL) {
		    if (max < high)
			    max = high;
		    return;
	    }
	    high++;
	    traveler(root->left, high);
	    traveler(root->right, high);

    }
    int max;
};
