// Find Minimum in Rotated Sorted Array II
// Find Minimum in Rotated Sorted Array 
// Search in Rotated Sorted Array
// Search in Rotated Sorted Array II 

//这四道题是一种类型的题目，可以套用一个框架


class Solution {
public:
	int findMin(vector<int> &num) {

		int first = 0, last = num.size() - 1;
		int mid;
		int min = num[0];

		while (first <= last) {

			mid = (first + last) / 2;

			if (num[first] < num[mid]) {
				if (num[first] < min){
					min = num[first];
				}
				first = mid+1;
			}
			else if (num[first] > num[mid]) {
				if (num[mid] < min) {
					min = num[mid];
				}
				last = mid - 1;
			}
			else {
				if (num[mid] < min) {
					min = num[mid];
				}
				first++;
			}
		}
		return min;
	}
};
