#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

class Solution {
public:
	int lengthOfLongestSubstring(string s) {
		map<char, int> visited;
		int max = 0;
		int index;

		for (int i = 0; i < s.size(); i++) {
			if (visited.find(s[i]) != visited.end()) {
				max = max < visited.size() ? visited.size() : max;

				index = visited[s[i]];
				for (auto it = visited.begin(); it != visited.end();) {
					if (it->second < index) {
						it = visited.erase(it);
					}
					else 
						it++;
				}
			}
			visited[s[i]] = i;
		}

		max = max < visited.size() ? visited.size() : max;
		return max;
	}
};



// Wrong Solution 
//
class Solution_Wrong {
public:
	int lengthOfLongestSubstring(string s) {

		vector<int> visited(256, -1);
		int max = 0;
		int ch, tmp_len;
		int start = 0;

		for (int i = 0; i < s.size(); i++) {
			ch = (int)s[i];
			if (visited[ch] < start) {
				visited[ch] = i;
				tmp_len = i - start + 1;
				max = max < tmp_len ? tmp_len : max;
				cout << "max = " << max << " i = " << i << " start = " << start<< endl;
			}
			else {
				start = visited[ch] + 1;
				// update visited array
				for (int k = start; k <= i; k++) 
					visited[s[k]] = start-1;
				//for loop end

				tmp_len = (i - start) + 1;
				max = max < tmp_len ? tmp_len : max;
				cout << "else max = " << max << " i = " << i << "start = " << start << endl;
			}			
		}
		return max;
	}

};


int main()
{
	Solution sol;

	string s = "qpxrjxkltzyx";

	cout << sol.lengthOfLongestSubstring(s) << endl;

	return 0;
}
