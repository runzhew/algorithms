class Solution {
public:
	vector<vector<int> > generateMatrix(int n) {


		if (n == 0) 
			return vector<vector<int> >();

		int count = 1, max = n*n;
		vector<vector<int> > result(n, vector<int>(n));

		int beginx = 0, endx = n-1;
		int beginy = 0, endy = n-1;
		int i;

		while (true) {
			// from left to right
			for (i = beginx; i <= endx; i++) {
				result[beginy][i] = count;
				count++;
			}
			if (++beginy > endy) {
				break;
			}
			// from top to buttom
			for (i = beginy; i <= endy; i++) {
				result[i][endx] = count;
				count++;
			}
			if (--endx < beginx)
				break;
			// from right to left 
			for (i = endx; i >= beginx; i--) {
				result[endy][i] = count;
				count++;
			}
			if (--endy < beginy) 
				break;
			// from buttom to top 
			for (i = endy; i >= beginy; i--) {
				result[i][beginx] = count;
				count ++;
			}
			if (++beginx > endx)
				break;
		}
		return result;
	}
};
