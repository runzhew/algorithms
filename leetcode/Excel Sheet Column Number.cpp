#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

class Solution {
public:
	int titleToNumber(string s) {

		reverse(s.begin(), s.end());

		int len = s.size();
		//firstly, take the last 'Z', sum += ('Z'-'A'+1)
		int mul = (s[0] - 'A') + 1;
		
		//then for the remaining "AB", set a carry,
		for (int i = 1; i < len; i++) {
			int tmp = ((s[i] - 'A') + 1);
			int carry = 1;
			//loop (carry * 26); add then  * ('B'-'A'+1)
			//it helps omit some if cases
			for (int j = 0; j < i; j++) {
				carry = carry * 26; 
			}
			mul += tmp * carry;
		}

		return mul;
	}
};

int main()
{
	Solution sol;
	cout << sol.titleToNumber("AZ") << endl;
	cout << sol.titleToNumber("BZ") << endl;
	cout << sol.titleToNumber("AT") << endl;

	return 0;
}
