#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

class Solution {
public:
	string multiply(string num1, string num2) { 
		
		if (num1.size() == 0 || num2.size() == 0) 
			return 0;

		int n1 = num1.size(); int n2 = num2.size();
		int carry = 0;
		string res(n1+n2+1, '0');
		int k = 0; // index of res
		
		reverse(num1.begin(), num1.end());
		reverse(num2.begin(), num2.end());

		for (int i = 0; i < n2; i++) {
			k = i;
			for (int j = 0; j < n1; j++) {
				int t1 = (num2[i]-'0') * (num1[j]-'0') + carry + (res[k] - '0');
				carry = 0; // reset 
				if (t1 >= 10) {
					carry = t1 / 10;
					t1 = t1 % 10;
				}

				res[k] = t1 + '0';
				k++;
			}
			//cout << res << endl;
			if (carry != 0) {
				res[k] = carry + '0';
				carry = 0;
			}
		}

		// just trim the result such as : 00001223 
		reverse(res.begin(), res.end());
		int index = 0;
		while (res[index] == '0' && index < res.size()) {
			index++;
		}
		if (index >= res.size()) 
			return "0";
		return res.substr(index, res.size()-index);
	}
};

int main()
{
	Solution sol;
	string s1 = "0";
	string s2 = "0";

	cout << sol.multiply(s1, s2) << endl;

	return 0;
}
