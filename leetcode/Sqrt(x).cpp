#include <iostream>

using namespace std;

class Solution {
public:
	int sqrt(int x) {

		int l = 1, r = x/2 + 1;

		if (x == 0) return 0;

		while (l <= r) {
			int m = (l+r)/2;
			if (x/m >= m && x/(m+1) < (m+1)) {
				return m;
			}

			if (x/m < m) {
				r = m-1;
			}
			else {
				l = m+1;
			}
		}
	}
};


int main(void)
{
	Solution sol;
	cout << "5 : " << sol.sqrt(5) << endl;
	cout << "8 : " << sol.sqrt(8) << endl;
	cout << "9 : " << sol.sqrt(9) << endl;

	return 0;
}
