class Solution {
public:
	int minDistance(string word1, string word2) {

		int n = word1.size();
		int m = word2.size();

		int ret[n+1][m+1];

		// initalize
		for (int i = 0; i < n+1; i++) 
			ret[i][0] = i;
		for (int j = 0; j < m+1; j++) 
			ret[0][j] = j;

		// calculate
		for (int i = 1; i < n+1; i++) {
			for (int j = 1; j < m+1; j++) {
				if (word1[i-1] == word2[j-1]) {
					ret[i][j] = ret[i-1][j-1];
				}
				else {
					ret[i][j] = min(ret[i-1][j-1], ret[i-1][j], ret[i][j-1]) + 1;
				}
			}
		}

		return ret[n][m];
	}

	int min(int a, int b, int c)
	{
		int t;
		if (a < b) t = a;
		else t = b;

		if (t < c) return t;
		
		return c;
	}
};
