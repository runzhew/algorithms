class Solution {
public:
	int romanToInt(string s) {
		int map[26];
		int i, n = s.size();
		int total = 0;

		map['I'-'A'] = 1; map['V'-'A'] = 5; map['X'-'A'] = 10; map['L'-'A'] = 50;
		map['C'-'A'] = 100; map['D'-'A'] = 500; map['M'-'A'] = 1000;

		s.push_back(s[n-1]);

		for (i = n-1; i >= 0; i--) {
			if (map[s[i] - 'A'] >= map[s[i+1] - 'A']) {
				total += map[s[i] - 'A'];
			}
			else {
				total -= map[s[i] - 'A'];
			}
		}

		return total;

	}
};
