#include <iostream>
#include <vector>
#include <string>

using namespace std;

void dump(const vector<vector<char> > &board)
{
	for (int i = 0; i < board.size(); i++) {
		for (int j = 0; j < board[0].size(); j++) {
			cout << board[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

class Solution {
public:
	Solution():solved(false){}

	void solveSudoku(vector<vector<char> > &board) {
		if (board.size() == 0)
			return ;

		int n = board.size();
		int i, j, k;
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				if (board[i][j] == '.') {
					for (k = 1; k <= 9; k++) {
						board[i][j] = '0'+k;
						if (isValid(board, i, j) == true) {
							//dump(board);
							solveSudoku(board);
							
					//=========================
							if (solved)
								return;
					//=========================
						}  // 重点关注 注释区域这些判断递归结束的代码
						   //  在递归判断结束这块仍然很薄弱
					}
					//=========================
					if (k > 9) {
						board[i][j] = '.';
						return;
					}
					//=========================
				}
			}
		}
		//=========================
		solved = true;
		//=========================
        
	}
private:
	bool solved;
	bool isValid(const vector<vector<char> > &board, int x, int y) {
		int i, j;
		for (i = 0; i < 9; i++) 
			if (i != x && board[i][y] == board[x][y])
				return false;
		for (j = 0; j < 9; j++) 
			if (j != y && board[x][j] == board[x][y])
				return false;
		for (i = 3 * (x / 3); i < 3 * (x / 3 + 1); i++)
			for (j = 3 * (y / 3); j < 3 * (y / 3 + 1); j++)
				if ((i != x || j != y) && board[i][j] == board[x][y])
					return false;
		return true;
	}

};




int main()
{
	string s = "..9748...";
	vector<char> v(s.begin(), s.end());
	vector<vector<char> > board;
	board.push_back(v);

	s = "7........";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	s = ".2.1.9...";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	s = "..7...24.";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	s = ".64.1.59.";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	s = ".98...3..";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	s = "...8.3.2.";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	s = "........6";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	s = "...2759..";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	dump(board);
	
	Solution  sol;

	sol.solveSudoku(board);

	dump(board);

	return 0;
}

