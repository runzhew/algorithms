#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <functional>

using namespace std;

class Solution {
public:
	vector<string> findRepeatedDnaSequences(string s) {

		set<int> save;
		set<string> result;
		hash<string> hashed; 
		vector<string> res;
		string tmp;
		
		if (s.size() < 10) return res;

		for (int i = 0; i <= s.size()-10; i++) {
			tmp = s.substr(i, 10);
			if (save.find(hashed(tmp)) == save.end()) {
				save.insert(hashed(tmp));
				continue;
			}
			else {
				result.insert(tmp);
			}
		}

		set<string>::iterator it = result.begin();
		for (; it != result.end(); it++)
			res.push_back(*it);

		return res;
	}
};
int main()
{
	Solution sol;
	vector<string> res = sol.findRepeatedDnaSequences("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT");
	for (int i = 0; i < res.size(); i++) {
		cout << res[i] << endl;
	}

	return 0;
}
