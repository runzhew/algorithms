class Solution {
public:
	vector<int> searchRange(int A[], int n, int target) {

		int start = 0;
		int end   = n;
		int mid;

		vector<int> range(2, -1);

		// search for lower bound
		while (start < end) {
			mid = (start + end) / 2;
			if (A[mid] < target) {
				start = mid+1;
			}
			else 
				end = mid;
		}
		if (A[start] != target)
		    return range;
		    
		range[0] = start;

		// search for upper bound 
		start = 0;
		end   = n;
		while (start < end) {
			mid = (start + end) / 2;
			if (A[mid] <= target) {
				start = mid+1;
			}
			else 
				end = mid;
		}

        // 如果range[0]不为-1 ，则肯定有up bound， 所以这里不需if判断
		range[1] = end-1;

		return range;
	}
};
