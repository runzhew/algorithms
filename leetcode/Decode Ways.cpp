#include <iostream>
#include <vector>
#include <math.h>
#include <stdlib.h>

using namespace std;

class Solution {
public:
    int numDecodings(string s) {
        
        if (s.size() == 0 || s[0] == '0') {
            return 0;
        }
        
        string ss = "0" + s;
        int n = ss.size();
        
        vector<int> note(n+1, 0);
        
        note[0] = 1;
        note[1] = 1;
        
        for (int i = 2; i < n; i++) {
            if (ss[i] != '0') {
                note[i] = note[i-1];
            }
            
            // 101 结果是 1， 因为 01 不能算作合法，所以要判断 ss[i-1] != '0'
            if (ss[i-1] != '0') {
                int t = atoi(ss.substr(i-1, 2).c_str());
                if ( t >= 1 && t <= 26) {
                    note[i] += note[i-2];
                }
            }
        }
        
        return note[n-1];
    }
};

int main()
{
	Solution sol;
	cout << sol.numDecodings("101") << endl;

	return 0;
}
