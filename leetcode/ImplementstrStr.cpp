#include <iostream>

using namespace std;

class Solution {
public:
	char *strStr(char *haystack, char *needle) {
		if (needle == NULL || haystack == NULL) {
			return NULL;
		}
		
		// these two if() is just for the strange test case of OJ
		if (*needle == '\0' && *haystack == '\0') {
			return haystack;
		}
		if (*needle == '\0') {  
		    return haystack;
		}

		char *p = haystack, *q = needle;
		char *head = p;

		while (*p != '\0') {

			if (*p != *q) {
				p++;
				continue;
			}
			
			head = p;
			
			while (*q != '\0') {
				if (*head == *q) {
					head++;
					q++;
				}
				else {
					q = needle;
					break;
				}
			}
			if (*q == '\0') {
				return p;
			}
			
			// 不加这句超时 。。。。。
			
			if (*head == '\0' && *q != '\0') {
				return NULL;
			}
			
			p++;
		}
		return NULL;
	}
};


int main()
{
	Solution sol;
	char haystack
	cout << sol.strStr("mississippi", "issip") << endl;
	return 0;
}


