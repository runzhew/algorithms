#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

class Solution {
public:
	int compareVersion(string version1, string version2) {

		vector<int> v1, v2;

		int prev = 0, cur = 0;
		stringstream ss;
		int value;

		version1 += ".";
		version2 += ".";
		while (cur <= version1.size()) {
			if (version1[cur] == '.') {
				string tmp = version1.substr(prev, cur-prev);
				//cout << "cur = " << cur << "tmp = " << tmp << endl;
				ss << tmp;
				ss >> value;
				v1.push_back(value);
				//cout <<"v1 = " << value << endl;
				ss.clear();
				cur++;
				prev = cur;
			}
			else {
				cur++;
			}
		}

		ss.clear();

		// do the same on version2 
		prev = 0; cur = 0;
		while (cur <= version2.size()) {
			if (version2[cur] == '.') {
				string tmp = version2.substr(prev, cur-prev);
				//cout << "tmp = " << tmp << endl;
				ss << tmp;
				ss >> value;
				v2.push_back(value);
				//cout << version2<<  "  v2 = " << value << endl;
				ss.clear();
				cur++;
				prev = cur;
			}
			else {
				cur++;
			}
		}

		// compare 
		int i, j;
		for (i = 0, j = 0; i < v1.size() && j < v2.size(); i++, j++) {
			if (v1[i] == v2[j]) 
				continue;
			if (v1[i] > v2[j]) 
				return 1;
			else 
				return -1;
		}

		//cout << "i = " << i << " , j = " << j << endl; 

		if (i != v1.size()) {
			if (v1[i] ==0) { // in case  (1.0   1)
				return 0;
			}
			return 1;
		}
		if (j != v2.size()) {
			if (v2[j] == 0) {
				return 0;
			}
			return -1;
		}

		// i == j
		return 0;
	}
};


int main()
{
	Solution sol;
	string s1 = "1.0";
	string s2 = "1";

	cout << sol.compareVersion(s1, s2) << endl;

	return 0;
}
