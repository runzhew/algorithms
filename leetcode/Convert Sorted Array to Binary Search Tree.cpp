#include <iostream>
#include <vector>

using namespace std;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    TreeNode *sortedArrayToBST(vector<int> &num) 
    {
        TreeNode *root = NULL;
        
        dfs(&root, num, 0, num.size()-1);
        
        return root;
    }
    
    void dfs(TreeNode **root, vector<int> &num, int start, int end)
    {
        if (start > end) {
            return ;
        }
        
        int mid = (start+end) / 2;
        *root = new TreeNode(num[mid]);
        
        dfs(&(*root)->left, num, start, mid-1);
        dfs(&(*root)->right, num, mid+1, end);
    }
};

class Solution_old {
public:

	TreeNode *sortedArrayToBST(vector<int> &num) {
		TreeNode * root = NULL;
		int start = 0, end = num.size();
		
		buildBST(root, num, start, end-1); 

		return root;
	}
private:
	void buildBST(TreeNode *&root, vector<int> &num, int start, int end) {
		if (start > end) {
			root = NULL;
			return;
		}

		int mid = (start + end) / 2;

		root = new TreeNode(0);
		root->val = num[mid];
		buildBST(root->left, num, start, mid-1);
		buildBST(root->right, num, mid+1, end);
	}
};


int main()
{
	Solution sol;

	return 0;
}
