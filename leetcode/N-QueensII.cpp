#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
	int totalNQueens(int n) {

		count = 0;
		vector<vector<bool> > matrix(n, vector<bool>(n, false));
		dfs(0, matrix, n);
		return count;
	}
private:

	int count;

	void dfs(int x, vector<vector<bool> > &matrix, int n) {
		if (x == n) {
			count++;
			return ;
		}

		for (int i = 0; i < n; i++) {
			if (check(x, i, matrix, n) == true) {
				matrix[x][i] = true;
				//cout << "(" << x << " , " << i << ") is true, \
					next line: " << x+1 << endl;
				dfs(x+1, matrix, n);

				// IMPORTANT
				// after the next DFS return, we should set 
				// the matrix[x][i] to be false;
				matrix[x][i] = false;
			}
		}
	}


	const bool check(int x, int y, vector<vector<bool> > &matrix, int n) const {
		int i, j;


		// bottom to top
		for (i = x; i >= 0; i--) {
			if (matrix[i][y] == true) {
				return false;
			}
		}
		// left to top right 
		for (i = x, j = y; (i >= 0) && (j < n); i--, j++) {
			if (matrix[i][j] == true) {
				return false;
			}
		}
		// right to top left
		for (i = x, j = y; (i >= 0) && (j >= 0); i--, j--) {
			if (matrix[i][j] == true) {
				return false;
			}
		}

		// check whether the x/y is too big that escape the "for" cases 
		if (x < n && x >= 0 && y <n && y >= 0) {
			return true;
		}
		else {
			return false;
		}
	}
};


int main()
{
	Solution sol;
	cout << sol.totalNQueens(8) << endl;

	return 0;
}
