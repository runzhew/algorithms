/**
 * Definition for singly-linked list with a random pointer.
 * struct RandomListNode {
 *     int label;
 *     RandomListNode *next, *random;
 *     RandomListNode(int x) : label(x), next(NULL), random(NULL) {}
 * };
 */
class Solution {
public:
	RandomListNode *copyRandomList(RandomListNode *head) {
		if (head == NULL) 
			return head;
		// copy the new node
		RandomListNode * cur = head;
		while (cur != NULL) {
			RandomListNode *tmp = new RandomListNode(cur->label);
			tmp->next = cur->next;
			cur->next = tmp;
			cur = tmp->next;
		}

		// copy the random pointer 
		cur = head;
		while (cur != NULL) {
			if (cur->random != NULL) {
				cur->next->random = cur->random->next;
			}
			cur = cur->next->next;
		}

		// detach the new list 
		RandomListNode *new_head = head->next;
		RandomListNode *dup = new_head;
		cur = head;
		while(dup != NULL) {
			cur->next = dup->next;
			cur = cur->next;
			if (cur != NULL) {
				dup->next = cur->next;
				dup = dup->next;
			}
			else {
				break;
			}
		}

		return new_head;
	}
};
