class Solution {
public:
	int jump(int A[], int n) {

		int max_reach_pos = 0;
		int last = 0;
		int step = 0;

		for (int i = 0; i < n-1; i++) {

			if (i > last) {
				step++;
				last = max_reach_pos;
			}

			if (i <= last) {
				if (i + A[i] > max_reach_pos) {
					max_reach_pos = i + A[i];
				}
				
				if (max_reach_pos >= n-1) 
					    return step+1; // 注意 + 1
			}
		}

		if (max_reach_pos >= n-1)
			return step;
		return 0;
	}
};
