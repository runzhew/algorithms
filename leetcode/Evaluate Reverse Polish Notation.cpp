#include <iostream>
#include <stack>
#include <vector>
#include <string>
#include <stdlib.h>

using namespace std;

class Solution {
public:
	int evalRPN(vector<string> &tokens) {
		if (tokens.size() == 0)
			return 0;

		stack<int> value;	

		for (int i = 0; i < tokens.size(); i++) {
			int a, b, t;

			if ((tokens[i][0] == '-' && tokens[i].size()>1) //negative number
			    || (tokens[i][0] >= '0' && tokens[i][0] <= '9')) {//positive number 

				t = atoi(tokens[i].c_str());
				value.push(t);
				continue;
			}

			switch(tokens[i][0]) {
			case '+': a = value.top(); value.pop();
				  b = value.top(); value.pop();
				  value.push(a+b);
				  break;
			case '-': a = value.top(); value.pop();
				  b = value.top(); value.pop();
				  value.push(b-a);
				  break;

			case '*': a = value.top(); value.pop();
				  b = value.top(); value.pop();
				  value.push(a*b);
				  break;

			case '/': a = value.top(); value.pop();
				  b = value.top(); value.pop();
				  value.push(b/a);
				  break;
			}
		}
		return value.top();
	}
};

int main() {
	Solution sol;
	vector<string> v ; //["-1","1","*","-1","+"]
	v.push_back("-1");
	v.push_back("1");
	v.push_back("*");
	v.push_back("-1");
	v.push_back("+");

	cout << sol.evalRPN(v) << endl;
	return 0;
}
