class Solution {
public:
	vector<vector<int> > subsetsWithDup(vector<int> &S) {

		vector<vector<int> > result;
		vector<int> tmp;

		sort(S.begin(), S.end());
		
		result.push_back(tmp);

		dfs(0, S, result, tmp);

		return result;
	}

	void dfs(int start, vector<int> &s, vector<vector<int> > &result,
		 vector<int> &tmp) {

		for (int i = start; i < s.size(); i++) {
			if (i > start && s[i] == s[i-1]) {
				continue;
			}
			
			tmp.push_back(s[i]);
			result.push_back(tmp);
			dfs(i+1, s, result, tmp);
			tmp.pop_back();
		}
	}
			
};
