#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
	bool isValidSudoku(vector<vector<char> > &board) {

		int rows = board.size();
		int cols = board[0].size();

		int i, j;

		// check for every rows
		for (i = 0; i < rows; i++) {
			initCheckBoard();
			for (j = 0; j < cols; j++) {
				if (check(board[i][j]) == false)
					return false;
			}
		}
		// check for every cols 
		for (i = 0; i < rows; i++) {
			initCheckBoard();
			for (j = 0; j < cols; j++) {
				if (check(board[j][i]) == false)
					return false;
			}
		}
		// check every 9 elements
		int l = 0, lm = 3;
		int c = 0, cm = 3;

		for (; lm <= 9; l+=3, lm+=3){
			// remember to reset c and cm
			c = 0;
			cm = 3;
			// remember to reset c and cm

			for (; cm <= 9; c+=3, cm+=3) {
				initCheckBoard();

				//cout << "l = " << l << " lm = " << lm << endl;
				//cout << "c = " << c << " cm = " << cm << endl;
				//cout << endl;

				for (i = l; i < lm; i++) {
					for (j = c; j < cm; j++) {
						if (check(board[i][j]) == false) 
						    return false;
					}
				}
			}
		}

		return true;

	}

private:
	void initCheckBoard()
	{
		for (int i = 0; i < 10; i++) {
			checkboard[i] = false;
		}
	}

	bool check(char ch) {
		if (ch == '.') 
		       return true;	

		if (checkboard[ch - '1'])
			return false;

		checkboard[ch - '1'] = true;
		return true;
	}

	bool checkboard[10];
};


int main()
{
	string s = "......5..";
	vector<char> v(s.begin(), s.end());
	vector<vector<char> > board;
	board.push_back(v);

	s = ".........";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);
	board.push_back(v);

	s = "93..2.4..";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	s = "..7...3..";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	s = ".........";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	s = "...34....";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	s = ".....3...";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	s = ".....52..";
	std::copy(s.begin(), s.end(), v.begin());
	board.push_back(v);

	
	Solution  sol;

	sol.isValidSudoku(board);

	for (int i = 0; i < board.size(); i++) {
		for (int j = 0; j < board[0].size(); j++) {
			cout << board[i][j] << " ";
		}
		cout << endl;
	}

	return 0;
}
