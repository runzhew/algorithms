class Solution {
public:
	string minWindow(string S, string T) {

		map<char, int> note;
		map<char, int> appear;
		
		if (S.size() < T.size()) {
		    return "";
		}

		for (int i = 0; i < T.size(); i++) {
			if (note.find(T[i]) != note.end()) {
				note[T[i]]++;
			}
			else {
				note[T[i]] = 1;
			}
		}

		int start = 0, end = 0, min_start = 0;
		int counter = 0;
		int min = INT_MAX;
		while (end < S.size()) {
			if (note.find(S[end]) != note.end()) {
				if (note[S[end]] > 0) {
					counter++;
				}
				note[S[end]] --;
			}
			if (counter == T.size()) {
				// move start to jiabi
				while (start < end) {
					if (note.find(S[start]) != note.end() &&
					    note[S[start]] < 0) {
						note[S[start]] ++;
						start++;
					}
					else if (note.find(S[start]) == note.end()) {
						start++;
					}
					else {
						break;
					}
				}

				if (end-start+1 < min) {
					min = end-start+1;
					min_start = start;
				}
				//counter = 0;
			}
			end++;
		}
		
		if (min == INT_MAX) { // no result
		    return "";
		}
		
		return S.substr(min_start, min);
	}
};
