class Solution {
public:
	bool canJump(int A[], int n) {

		int max_reach_pos = 0; //当前能到达的最远位置

		for (int i = 0; i < n-1; i++) {
			if (A[i] != 0 && i + A[i] > max_reach_pos) {
				max_reach_pos = i + A[i];
			}
			else if (A[i] == 0) {
				if (max_reach_pos <= i) 
					return false;
			}

			if (max_reach_pos >= n-1)
				return true;
		}

		if (max_reach_pos >= n-1)
			return true;
		return false;

	}
};
