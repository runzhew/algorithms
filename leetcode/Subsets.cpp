#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

class Solution {
public:
	vector<vector<int> > subsets(vector<int> &S) {
		vector<vector<int> > result;
		vector<int> tmp;

		// for empty element
		result.push_back(tmp);

		if (S.size() == 0)
			return result;

		sort(S.begin(), S.end());
		dfs(0, tmp, result, S);

		return result;
	}
private:
	void dfs(int start, vector<int> &tmp, vector<vector<int> > &result, 
		 vector<int> &S) {
		if (start == S.size()) {
			return;
		}

		for (int i = start; i < S.size(); i++) {
			tmp.push_back(S[i]);
			result.push_back(tmp);
			dfs(i+1, tmp, result, S);
			tmp.pop_back();
		}
	}
};

int main()
{
	Solution sol;
	vector<int> S;
	vector<vector<int> > ret;
	S.push_back(1);
	S.push_back(2);
	S.push_back(3);

	ret = sol.subsets(S);

	for (int i = 0; i < ret.size(); i++){
		for (int j = 0; j < ret[i].size(); j++) {
			cout << ret[i][j] << " , ";
		}
		cout << endl;
	}

	return 0;
}
