class Solution {
	public:
		int lengthOfLastWord(const char *s) {

			int i = 0;
			int len = 0;
			while (s[i] != '\0') {
				i++;
			}
			i--; // last slot

			while (s[i] == ' ') {
				i--;
			}

			// now meet the first char 
			while (i >= 0 && s[i] != ' ') {  // pay attention to i >=0
				i--;
				len++;
			}

			return len;
		}
};
