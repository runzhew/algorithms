/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	bool isValidBST(TreeNode *root) {

		if (root == NULL)
			return true;

		if(root->left != NULL && root->right != NULL) {
			if (root->val > root->left->val && root->val < root->right->val) {
				if(isValidBST(root->left)) 
					return true;
				if(isValidBST(root->right))
					return true;
			}
			return false;
		}

		if (root->left != NULL && root->right == NULL) {
			if (root->val > root->left->val) {
				if (isValidBST(root->left))
				    return true;
			}
			 
			return false;
		}

		if (root->left == NULL && root->right != NULL) {
			if (root->val < root->right->val) {
				if(isValidBST(root->right))
					return true;
			}
			return false;
		}

		if(root->left == NULL && root->right == NULL) {
			return true;
		}
	}
};
