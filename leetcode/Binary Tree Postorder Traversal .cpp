/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution {
public:
	vector<int> postorderTraversal(TreeNode *root) {

		TreeNode * pre = NULL;  // 指向刚刚访问过的节点
		TreeNode * cur;   // 指向当前节点  
		vector<int> result;

		stack<TreeNode *> st;
		if (root == NULL) 
		    return result;
		    
		st.push(root);

		while (!st.empty()) {

			cur = st.top(); 
			
		    //如果当前节点没有左右孩子，或者有左孩子或右孩子，但已经被访问，  
            //则直接输出该节点，将其出栈，将其设为上一个访问的节点  
			if ((cur->left == NULL && cur->right == NULL) || 
			    (pre != NULL) && (cur->left == pre || cur->right == pre)) {

			    result.push_back(cur->val);
			    st.pop();
			    pre = cur;
			}
			else {
			    //如果不满足上面两种情况,则将其右孩子先入栈
				if (cur->right != NULL) {
					st.push(cur->right);
				}
				if (cur->left != NULL) { // 然后左孩子入栈 
					st.push(cur->left);
				}
			}
		}

		return result;
	}
};
