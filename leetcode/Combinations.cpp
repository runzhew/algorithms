#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
	vector<vector<int> > combine(int n, int k) {
		vector<vector<int> > result;
		vector<int > path;

		dfs(n, k, 1, 0, path, result);

		return result;
	}

private:
	void dfs(int n, int k, int start, int cur, vector<int> &path,
		 vector<vector<int> > &result) {

		if (cur == k) {
			result.push_back(path);
		}

		for (int i = start; i <= n; i++) {
			path.push_back(i);
			dfs(n, k, i+1, cur+1, path, result);
			cout << "i+1= " << i+1 << ", cur+1=" << cur+1 << endl; 
			path.pop_back();
		}
	}

};

void dump(vector<vector<int> > v) 
{
	for (int i = 0; i < v.size(); i++) {
		for (int j = 0; j < v[i].size(); j++) {
			cout << v[i][j] << ", " ;
		}
		cout << endl;
	}
}

int main(void)
{
	Solution sol;
	dump(sol.combine(4, 2));

	return 0;
}
