class Solution {
public:
    vector<int> rightSideView(TreeNode *root) {
        
        vector<int> result;
        
        if (root == NULL) {
            return result;
        }
        
        queue<TreeNode *> parent;
        queue<TreeNode *> child;
        TreeNode * record_last;
        
        parent.push(root);
        
        while (!parent.empty()) {
            while (!parent.empty()) {
                TreeNode * t = parent.front();
                record_last = t;
                parent.pop();
                
                if (t->left) {
                    child.push(t->left);
                }
                if (t->right) {
                    child.push(t->right);
                }
            }
            parent.swap(child);
            //child.clear();
            result.push_back(record_last->val);
        }
        
        return result;
    }
};
