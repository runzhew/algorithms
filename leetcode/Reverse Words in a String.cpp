#include<iostream>
#include<string>
using namespace std;

class Solution {
	public:
	void reverseWords(string &s) {
		int i = 0;
		int begin = 0;
	       	int end = s.size() - 1;

		string tmp;

		// deal with space at front and end. e.g. "  xyz  "
		while (s[begin] == ' ' && begin <= end) {
			begin++;
		}

		while (s[end] == ' ' && begin <= end) {
			end--;
		}

		if (begin > end) {
			s.clear();
			return;
		}
		// end of dealing with space at front and end. e.g. "  xyz  "
		// copy the effective substring
		tmp.assign(s, begin, end-begin+1); 
	

		reverse(tmp, 0, (tmp.size() - 1));

		begin = 0;
		end = 0;

		for (i = 0; i < tmp.size();) {
			while ((tmp[i] != ' ') && (i < tmp.size()) ) {
				i++;
			}
			end = i - 1;
			reverse(tmp, begin, end);

			// redundent the spaces of tmp e.g. "   a    b " 
			while(tmp[i] == ' ') {
				i++;
			}
			if (i > end+2) {
				int step = i-(end-2);
				for (; i < tmp.size(); i++) {
					tmp[i-step] = tmp[i];
				}

				// how to use resize ?? remember it!
				tmp.resize(tmp.size()-step);
			}

			begin = end+2;
			i = begin;
		}

		s.swap(tmp);
	}
	private:
	void reverse(string &s, int left, int right) {

		while (left < right) {
			swap(&s[left], &s[right]);
			left++; right--;
		}
	}	

	void swap(char* a, char* b) {
		char tmp = *a;
		*a = *b;
		*b = tmp;
	}
};

int main()
{
	string s = " ";
	Solution sol;
	sol.reverseWords(s);
	std::cout << s << std::endl;
	return 0;
}
