class Solution {
public:
    vector<int> plusOne(vector<int> &digits) {
        
        if (digits.size() == 0) {
            return digits;
        }
        
        vector<int> result;
        int t;
        int carry = 1; // for the initial one
        
        for (int i = digits.size()-1; i >= 0; i--) {
            t = digits[i] + carry;
            carry = 0;
            if (t >= 10) {
                carry = 1;
                t %= 10;
            }
            result.push_back(t);
        }
        if (carry == 1) {
            result.push_back(1);
        }
        reverse(result.begin(), result.end());
        
        return result;
    }
};


class Solution {
	public:
		vector<int> plusOne(vector<int> &digits) {
			int plus = 0;
			int tmp;

			if (digits[digits.size() - 1] < 9) {
				digits[digits.size() - 1] += 1;
				return digits;
			}
			else {
				plus = 1;
				for (int i = digits.size() - 1; i >= 0; i--) {
					tmp = digits[i] + plus;
					plus = 0;
					if (tmp > 9) {
						digits[i] = tmp % 10;
						plus = 1;
					}
					else {
						digits[i] += 1;
						return digits;
					}
				}

				if (plus == 1) { //  999 + 1, need to reallocate memory
					vector<int> new_digits(digits.size() + 1);
					new_digits[0] = 1;
					return new_digits;
				}
			}

		}
};
