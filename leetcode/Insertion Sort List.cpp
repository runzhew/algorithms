/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
	ListNode *insertionSortList(ListNode *head) {
		ListNode dummy(INT_MIN);

		ListNode *sorted_tail = &dummy;
		ListNode *unsorted_head = head;
		ListNode *prev, *prev_next;

		while (unsorted_head != NULL) {
			ListNode *tmp = unsorted_head; // save a copy
			prev = find_position(&dummy, sorted_tail, unsorted_head->val);
			// find ! then change the position
			unsorted_head = unsorted_head->next;
			prev_next = prev->next;
			prev->next = tmp;
			tmp->next = prev_next;

			// keep sorted_tail is the true tail
			while (sorted_tail->next != NULL) {
				sorted_tail = sorted_tail->next;
			}
		}
		return dummy.next;
	}

	ListNode *find_position(ListNode *dummy, ListNode *sorted_tail, int val) 
	{
		ListNode *prev = dummy;
		while (prev->next != NULL) {
			if (prev->next->val < val) {
				prev = prev->next;
			}
			else {
				break;
			}
		}
		return prev;
	}
};
