/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */

class Solution {
public:
    vector<int> inorderTraversal(TreeNode *root) {
        stack<TreeNode*> st;
        vector<int> res;
        while (root != NULL) {
            st.push(root);
            root = root->left;
        }
        
        TreeNode *t;
        while (!st.empty()) {
            t = st.top();
            st.pop();
            res.push_back(t->val);
            
            if (t->right != NULL) {
                t = t->right;
                while (t != NULL) {
                    st.push(t);
                    t = t->left;
                }
            }
        }
        return res;
    }
};

class Solution {
public:
    vector<int> inorderTraversal(TreeNode *root) {
	    stack <TreeNode *> st;
	    vector <int> v;
	    TreeNode *p = root;

	    if (root == NULL) {
		    return v;
	    }

	    while (!st.empty() || p != NULL) {

		    if (p != NULL) {
			    st.push(p);
			    p = p->left;
		    }
		    else {
			    p = st.top();
			    st.pop();
			    v.push_back(p->val);
			    p = p->right;
		    }
	    }

	    return v;
    }

};

class Solution {
public:
    vector<int> inorderTraversal(TreeNode *root) {
        
        stack<TreeNode *> st;
        TreeNode *p = root;
        vector<int> result;
        
        if (root == NULL) {
            return result;
        }
        
        while (!st.empty() || p != NULL) {

            while (p != NULL) {
                st.push(p);
                p = p->left;
            }
            
            TreeNode *t = st.top();
            st.pop();
            result.push_back(t->val);
            
            if (t->right != NULL) {
                p = t->right;
            }
            else {
                p = NULL;
            }
        }
        
    }
};
