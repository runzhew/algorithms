class Solution {
public:
    int hammingWeight(uint32_t n) {
        int bit = 0;
        int count = 0;
        
        while ( n > 0) {
            bit = n & 0x1;
            if (bit == 1) {
                count++;
            }
            n = n >> 1;
        }
        return count;
    }
};
