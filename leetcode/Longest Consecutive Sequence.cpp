#include <iostream>
#include <set>
#include <vector>

using namespace std;

class Solution {
public:
	int longestConsecutive(vector<int> &num) {

		int max = 0;
	       	int left_len = 0, right_len = 0;
		set<int> myset;
		for (int i = 0; i < num.size(); i++) {
			myset.insert(num[i]);
		}

		for (int i = 0; i < num.size(); i++) {
			int ltmp, rtmp;
			if (myset.find(num[i]) != myset.end()) { //found

				ltmp = num[i];
				rtmp = num[i];

				myset.erase(num[i]);
				// left side / smaller side
				while (myset.find(--ltmp) != myset.end()) {
					left_len++;
					myset.erase(ltmp); // tmp has already minus 1
				}

				// right side / bigger side
				while (myset.find(++rtmp) != myset.end()) {
					right_len++;
					myset.erase(rtmp); // tmp has already minus 1
				}

				if (max < right_len + left_len + 1) {
					max = right_len + left_len + 1;
				}
			}

			right_len = 0;
			left_len = 0;
		}
		return max;
	}
};


int main(void) 
{
	Solution sol;
	vector<int> num;
	num.push_back(0);
	cout << sol.longestConsecutive(num) << endl;
	return 0;
}
