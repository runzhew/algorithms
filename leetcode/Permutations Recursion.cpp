class Solution {
public:
	vector<vector<int> > permute(vector<int> &num) {
		vector<vector<int> > result;
		vector<int> tmp;

		sort(num.begin(), num.end());

		dfs(num, result, tmp);

		return  result;
	}

	void dfs(vector<int> &num, vector<vector<int> > &result, vector<int> tmp)
	{
		if (tmp.size() == num.size()) {
			result.push_back(tmp);
			return;
		}
		
		for (int i = 0; i < num.size(); i++) {
			auto pos = find(tmp.begin(), tmp.end(), num[i]);
			if (pos == tmp.end()) {
				tmp.push_back(num[i]);
				dfs(num, result, tmp);
				tmp.pop_back();
			}
		}
	}
};
