/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class BSTIterator {
public:
	BSTIterator(TreeNode *root) {

		while (root != NULL) {
			s.push(root);
			root = root->left;
		}

	}

	/** @return whether we have a next smallest number */
	bool hasNext() {

		if (s.empty()) 
			return false;
		return true;

	}

	/** @return the next smallest number */
	int next() {

		TreeNode * t = s.top();
		s.pop();

		if (t->right != NULL) {
			TreeNode *p = t->right;

			while (p != NULL) {
				s.push(p);
				p = p->left;
			}
		}
		return t->val;
	}

private:
	stack<TreeNode *> s;
		
};

/**
 * Your BSTIterator will be called like this:
 * BSTIterator i = BSTIterator(root); 
 * while (i.hasNext()) cout << i.next();
 */
