#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

class Solution {
public:
	vector<string> restoreIpAddresses(string s) {

		if (s.size() < 4 || s.size() > 12)
			return  result;

		string tmp;
		vector<int> v;

		dfs(0, 3, s, v);

		return result;
	}

	vector<string> result;

	void dfs(int start, int dots, string &s, vector<int> &v)
	{
		if (dots == 0) {
			string tmp;
			if (check(s, v, tmp) == true) {
				for (int i = 0; i < v.size(); i++) 
					cout << v[i] << ", ";
				cout << endl;
				cout << "push " <<  tmp << endl;
				result.push_back(tmp);
			}
			return ;
		}

		int i;
		// the dot will put after s[i] element 
		for (i = start; i < s.size()-1; i++) {
			v.push_back(i);
			dfs(i+1, dots-1, s, v);
			v.pop_back();
		}
	}
	
	bool check(string &s, vector<int> &v, string &tmp)
	{
		stringstream ss;
		int num;

		tmp.clear();

		string s1 = s.substr(0, v[0]+1);
		string s2 = s.substr(v[0]+1, v[1]-v[0]);
		string s3 = s.substr(v[1]+1, v[2]-v[1]);
		string s4 = s.substr(v[2]+1, s.size()-1-v[2]);

		//cout << s1 << "," << s2 << "," << s3 << ", " << s4 << endl;

		ss << s1;
		ss >> num;
		//cout << s1 << "  " << num << endl;
		if (s1[0] == '0')
			if (s1.size() != 1)
				return false;
		if (num > 255) return false;
		// MUST clear() !!!!!!!!!!
		// MUST clear() !!!!!!!!!!
		// MUST clear() !!!!!!!!!!
		// MUST clear() !!!!!!!!!!
		// MUST clear() !!!!!!!!!!
		ss.clear();

		ss << s2;
		ss >> num;
		//cout << s2 << "  " << num << endl;
		if (s2[0] == '0')
			if (s2.size() != 1)
				return false;
		if (num > 255) return false;
		ss.clear();

		ss << s3;
		ss >> num;
		//cout << s3 << "  " << num << endl;
		if (s3[0] == '0')
			if (s3.size() != 1) 
				return false;
		if (num > 255) return false;
		ss.clear();

		ss << s4;
		ss >> num;
		//cout << s4 << "  " << num << endl;
		if (s4[0] == '0')
			if (s4.size() != 1) 
				return false;
		if (num > 255) return false;

		tmp = s1 + "." + s2 + "." + s3 + "." + s4;
		return true;
	}
};


int main()
{
	Solution sol;

	string s("19216811");
	string tmp;
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);

	if (sol.check(s, v, tmp) == true) {
	       cout << tmp <<endl;
	}
	else {
		cout << "check return false" << endl;
	}


	vector<string> ret = sol.restoreIpAddresses("19216811");

	/*
	vector<string>::iterator it = ret.begin();
	for (; it != ret.end(); it++) {
		cout << *it << endl;
	}
	*/
	return 0;
}
