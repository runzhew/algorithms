#include <stdio.h>

void swap(int *a, int *b)
{
	int t = *a;
	*a = *b ;
	*b = t;
}

void bubbleSort(int *array, int len)
{
	int i = len, j;

	while (i > 0) {
		for (j = 0; j < i - 1; j++) {
			if (array[j] > array[j + 1]) {
				swap(&array[j], &array[j + 1]);
			}
		}
		i--;
	}
}

void dump(int *array, int len)
{
	int i;
	for (i = 0; i < len; i++) {
		printf("%d, ", array[i]);
	}
	printf("\n");
}

int main(void)
{
	int arr[6] = {4, 2, 7, 9, 1, 5};

	dump(arr, 6);
	bubbleSort(arr, 6);
	dump(arr, 6);

	return 0;
}
