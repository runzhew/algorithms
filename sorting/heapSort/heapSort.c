// this algorithm is inspired by CLRS
#include "heapSort.h"

int heap_size = LEN;

int parent(int i)
{
	return (i - 1) / 2;
}

int left(int i)
{
	return 2 * i + 1;
}

int right(int i)
{
	return 2 * i + 2;
}

void swap(int *a, int *b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

void dump(int *array)
{
	int i;
	for (i = 0; i < LEN; i++) {
		printf("%d, ", array[i]);
	}
	printf("\n");
}

void max_heapify(int *array, int i)
{
	int largest = i;
	int l = left(i);
	int r = right(i);
	
	while (1) {

		largest = i;
		l = left(i);
		r = right(i);
		// should be < not <= , if the array starts from 0
		// this is different from CLRS code
		if ((l < heap_size) && (array[l] > array[i])) {
			largest = l;
		}

		// should be < rather than <= if the array starts from 0
		if ((r < heap_size) && (array[r] > array[largest])) {
			largest = r;
		}

		if (largest != i) {
			swap(&array[largest], &array[i]);
			i = largest;
		}
		else {
			break;
		}
	}
}

void build_max_heap(int *array)
{
	int i;
	for (i = heap_size/2; i >= 0; i--) {
		max_heapify(array, i);
	}
}

void heapSort(int *array)
{
	int i;
	build_max_heap(array);
	dump(array);

	for (i = heap_size - 1; i > 0; i--) {
		swap(&array[i], &array[0]);
		heap_size--;
		max_heapify(array, 0);
		printf("[0] = %d\n", array[0]);
		dump(array);
	}
}

