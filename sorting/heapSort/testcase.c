#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "heapSort.h"

int array[10] ={16, 4, 10, 14, 7,9,3,2,8,1};

int main()
{
	int i;

	// generate a random num
	srand(time(NULL));

	for (i = 0; i < LEN; i++) {
		//array[i] = rand() % 100;
	}

	printf("before heapSort:\n");
	for (i = 0; i < LEN; i++) {
		printf("%d, ", array[i]);
	}
	printf("\n\n");

	heapSort(array);

	printf("after heapSort:\n");
	for (i = 0; i < LEN; i++) {
		printf("%d, ", array[i]);
	}
	printf("\n\n");

	return 0;
}
