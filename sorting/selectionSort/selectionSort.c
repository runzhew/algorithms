#include <stdio.h>

void swap(int *a, int *b) 
{
	int t = *a;
	*a = *b;
	*b = t;
}

void selectionSort(int array[], int n)
{
	for (int i = 0; i < n; i++) {
		for (int j = i+1; j < n; j++) {
			if (array[i] > array[j]) {
				swap(&array[i], &array[j]);
			}
		}
	}
}

void dump(int array[], int n) 
{
	for (int i = 0; i < n; i++) {
		printf("%d, ", array[i]);
	}

	printf("\n");
}

int main()
{
	int array[] = {4,6,1,9,7,3,87,32,13,25};
	dump(array, 10);
	selectionSort(array, 10);
	dump(array, 10);
	return 0;
}

