#include "mergeSort.h"

void dump(int low, int high, int *array)
{
	int i;
	printf("dump, array[%d -- %d] are sorted:  ", low, high);
	for (i = 0; i < LEN; i++) {
		printf("%d, ", array[i]);
	}
	printf("\n");
}


void mergeSort(int *array, int *temp, int low, int high)
{
	int mid = (low + high) / 2;
	if (low < high) {
		mergeSort(array, temp, low, mid);
		mergeSort(array, temp, mid + 1, high);
		mergeArray(array, temp, low, mid, high);
	}
}

void mergeArray(int *array, int *temp, int low, int mid, int high)
{
	int i;

	// copy the element of array to temp
	for (i = low; i <= high; i++) {
		temp[i] = array[i];
	}

	int tLeft = low;
	int tRight = mid + 1;
	int current = low;

	while (tLeft <= mid && tRight <= high) {
	       if (temp[tLeft] < temp[tRight]) {
		       array[current] = temp[tLeft];
		       tLeft++;
	       }
	       else {
		       array[current] = temp[tRight];
		       tRight++;
	       }
	       current++;
	}

	// copy the rest of Left side element to the array,
	// we IGNORE the right side, why ?
	while (tLeft <= mid) {
		array[current++] = temp[tLeft++];
	}
	dump(low, high, array);
}


