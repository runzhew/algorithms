#ifndef _MERGESORT_H_
#define _MERGESORT_H_

#include <stdio.h>

#define LEN 10 

void mergeArray(int *array, int *temp, int low, int mid, int high);
void mergeSort(int *array, int *temp, int low, int high);

#endif 
