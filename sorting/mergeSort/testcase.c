#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mergeSort.h"

int array[100], temp[100];

int main()
{
	int i;

	// generate a random num
	srand(time(NULL));

	for (i = 0; i < LEN; i++) {
		array[i] = rand() % 100;
	}

	printf("before mergeSort:\n");
	for (i = 0; i < LEN; i++) {
		printf("%d,", array[i]);
	}
	printf("\n");

	mergeSort(array, temp, 0, LEN - 1);

	printf("after mergeSort:\n");
	for (i = 0; i < LEN; i++) {
		printf("%d,", array[i]);
	}
	printf("\n");

	return 0;
}
