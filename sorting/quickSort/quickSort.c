// the algorithm is inspired by CLRS 

void quickSort(int *array, int left, int right)
{
	int index;

	if (left >= right)
		return;

	index = partition(array, left, right);
	quickSort(array, left, index - 1);
	quickSort(array, index + 1, right);
}

void swap(int *a, int *b)
{
	int t = *a;
	*a = *b;
	*b = t;
}

int partition(int *array, int left, int right)
{
	int pivot = array[right];
	int pre = left - 1;
	int i;

	for (i = left; i < right; i++) {
		if (array[i] < pivot) {
			pre++;
			swap(&array[i], &array[pre]);
		}
	}
	pre++;
	swap(&array[pre], &array[right]);

	return pre;
}
