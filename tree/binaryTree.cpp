#include <iostream>
#include <string>
#include <stack>
#include <queue>

using namespace std;

struct node {
	char data;
	struct node *left;
	struct node *right;
};


int index = 0; // for buildTree

void buildTree(struct node* *root, char* data)
{
	char e = data[index++];
	if (e == '\0') 
		return;

	if (e == '#') {
		*root = NULL;
		return ;
	}
	else {
		*root = (node*) new node;
		(*root)->data = e;
		buildTree(&(*root)->left, data);
		buildTree(&(*root)->right, data);
	}
}

void depthFirstSearch(struct node* root)
{ 
	stack<struct node*> nodestack;
	nodestack.push(root);
	struct node *  nd;
	while (!nodestack.empty()) {
		nd = nodestack.top();
		cout << nd->data << endl;
		nodestack.pop();
		if (nd->right)
			nodestack.push(nd->right);
		if (nd->left)
			nodestack.push(nd->left);
	}
}

int main()
{
	char data[15] = {'a', 'b', 'd', '#', '#', 'e', '#', '#', 'c', 'f', '#', '#', 'g', '#', '#'};
	struct node * root;
	buildTree(&root, data);
	depthFirstSearch(root);
	return 0;
}
