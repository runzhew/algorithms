## 纪念

为了找工作刷了好久的leetcode...然而并没有什么卵用...    

好在以后不需要了 :-) 

## Advanced C++ functions & APIs

###Leetcode:

*  Search for a Range   

	`upper_bound()`,  `lower_bound()`, `distance()`


* 3Sum

	construct a vector by given numbers

	`result.push_back(vector < int > {num[cur], num[start], num[end]});`

	Construct a Matrix by using C++ 

	vector<vector<int> > matrix(n, vector<int>(n, 0));

* Divide Two Integers

	INT_MIN = -2147483648   		
	INT_MAX = 2147483647
	But, call abs(-2147483648) = 2147483648

* Single Number II

	位操作。 
	`(n & 1)  获得最后一位是0/1`  
	`n >> 1 n右移一位， 注意，结果不是移出的那个0或1，而是剩下的 n被移过以后的值` 
	
* Sort List

	一个单链表，如何找到它的中间节点？

	while (fast->next != NULL && fast->next->next != NULL)

* Longest Substring Without Repeating Characters 

```c
for (auto it = visited.begin(); it != visited.end();) {
	if (it->second < index) {
		it = visited.erase(it);  // hash 的迭代这里语法不熟
	}
	else 
		it++;
```

## Advanced Language Skills

