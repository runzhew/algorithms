#include <stdio.h>

int binary_search(int *array, int left, int right, int value)
{
	int l = left;
	int r = right - 1;
	int mid;

	while (l <= r) {
		mid = (r + l) / 2;
		if (array[mid] > value) {
			r = mid - 1;
		}
		else if (array[mid] < value) {
			l = mid + 1;
		}
		else {
			return mid;
		}
	}

	return -1;
}

int main(void)
{
	int array[7] = {1,2,3,4,5,6,7};

	int ret = binary_search(array, 0, 6, 5);

	printf("ret = %d\n", ret);
	return 0;
}
