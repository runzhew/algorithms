#include<iostream>
using namespace std;
 
int &fun()
{
    static int x = 10;
    return x;
}

int main()
{
    cout << fun() << endl;;
    fun() = 30;
    cout << fun() << endl;;
    fun() = 40;
    cout << fun() << endl;
    return 0;
}
