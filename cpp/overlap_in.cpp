#include <iostream>
#include <string>

using namespace std;

class Fruit
{
public:
	Fruit(const string &nst = "apple", const string &cst = "green"):name(nst),colour(cst){}

	~Fruit(){}

	friend ostream& operator << (ostream& os, const Fruit& f);  //输入输出流重载，不是类的成员，

	friend istream& operator >> (istream& is, Fruit& f);       // 所以应该声明为类的友元函数

private:
    string name;
	string colour;
};

ostream& operator << (ostream& os, const Fruit& f)
{
	os << "The name is " << f.name << ". The colour is " << f.colour << endl;
	return os;
}

istream& operator >> (istream& is, Fruit& f) 
{
	is >> f.name >> f.colour;
	if (!is)
	{
		cerr << "Wrong input!" << endl;
	}
	return is;
}

int main()
{
	Fruit apple;
	cout << "Input the name and colour of a kind of fruit." << endl;
	cin >> apple;      
	cout << apple;
	return 0;
}
