#include <iostream>

using namespace std;

class Test {
	private:
		int a, b;
	public:
		Test(int a, int b) {
			this->a = a;
			this->b = b;
		}

		Test& operator = (const Test &t) {
			this->a = t.a;
		        this->b = t.b;
			return *this;
		}	

		friend ostream & operator << (ostream &out, const Test &t); 
};

ostream & operator << (ostream &out, const Test &t) {
	out << "a = " << t.a << ", b = " << t.b << endl;
	return out;
}

int main()
{
	Test t(3, 4);
	Test t2(5, 6);
	cout << t << endl;
	t = t2;
	cout << t << endl;
	return 0;
}
