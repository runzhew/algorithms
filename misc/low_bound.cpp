#include <iostream>

using namespace std;

int main()
{  
	int lower = 0;
	int upper = 19;
	int mid;    
	int target = 5;

	int num[] = {1,2,3,4,5,5,5,5,8,8,8,8,8,8,8,8,8,9,9,10};

	while (lower < upper) {
		mid = (lower + upper) / 2;
		if (num[mid] < target)
			lower = mid + 1;
		else
			upper = mid;
	}

	cout << "low bound of 8 is : " << lower << endl;

	return 0;
}
