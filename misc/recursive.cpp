#include <iostream>

using namespace std;

int max(int a, int b) {
	return a > b ? a : b;
}

int max_length(string s, int start, int end)
{
	// only two char
	if (s[start] == s[end] && start+1 == end) 
		return 2;

	// only one char
	else if (start == end) 
		return 1;

	// s[start] == s[end]
	else if (s[start] == s[end]) {
		return max_length(s, start+1, end-1) + 2;
	}

	return max(max_length(s, start+1, end), max_length(s, start, end-1));
}


int main()
{
	string s = "BBABCBAB";
	int n = s.size();
	cout << "Max = " << max_length(s, 0, n) << endl;

	return 0;
}
